#pragma once

#include "hashtypes.h"

//Crea un insieme disgiunto di 1 elemento contenente d, ed aggiorna la tabella hash
set_t *setMake(int d);

//Ritorna un puntatore al Set di d, se esiste
set_t *setFind(int d);

//Ritorna un puntatore al rappresentante di d, se esiste
set_el_t *setRepFind(int d);

//Unisce gli insiemi a cui appartengono d1 e d2, inserendo il più piccolo nel più grande.
int setUnion(int d1, int d2, int weighted);

//Unisce n insiemi disgiunti
int setUnionMany(int *dv, size_t dim);

//Stampa gli elementi del Set S
set_t *setPrint(int d);

// Ritorna il contenuto dell'elemento e
int setGetData(set_el_t *e);

// Ritorna il rappresentante dell'elemento e
set_el_t *setGetRep(set_el_t *e);

//Crea un insieme disgiunto a rango che contiene d come elemento
rset_t *rsetMake(int d);

//Ritorna un puntatore al Rank Set a cui appartiene s, eventualmente "comprimendo" il percorso
rset_t *rsetFind(int d);

//Linka due set, in base al rango
rset_t *rsetLink(rset_t *e1, rset_t *e2);

//Unisce 2 set usando find e link. Ritorna -1 in caso di errore
rset_t *rsetUnion(int d1, int d2);

//Unisce n set a rango. Ritorna un codice di errore
int rsetUnionMany(int *dv, size_t dim);

//Stampa il Rank set a cui appartiene d (Potrebbe richiedere tempo!)
rset_t *rsetPrint(int d);

// Ritorna i dati in un elemento
int rsetGetData(rset_t *e);

// Ritorna il padre dell'elemento
rset_t *rsetGetFather(rset_t *e);

