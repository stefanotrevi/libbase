#pragma once

#include "rbttypes.h"

//Inizializza un albero red-black
void rbtInit(rbt_t *t);

//Elimina un RBT
void rbtDelete(rbt_t *T);

//Elimina n e tutti i nodi sotto di lui
void rbtDeleteNodes(rbt_t *T, rbt_node_t *n);

//Ruota a sinistra un nodo di un RBT
void rbtRotL(rbt_t *T, rbt_node_t *n);

//Ruota a destra un nodo di un RBT
void rbtRotR(rbt_t *T, rbt_node_t *n);

//Aggiunge un nuovo elemento ad un RBT
rbt_t *rbtPut(rbt_t *T, int n);

//Sistema un RBT dopo aver aggiunto un nuovo nodo
void rbtFix(rbt_t *T, rbt_node_t *n);

//Funzione di supporto per risistemare un RBT dopo aver aggiunto un nodo
void rbtBalance(rbt_t *T, rbt_node_t *n);

//Stampa un RBT in ordine
rbt_t rbtPrintOrd(rbt_t T);

//Parte ricorsiva della funzione print_order_rbt
void rbtPrintOrec(rbt_t T);

//Stampa un RBT in pre-order
rbt_t rbtPrintPre(rbt_t T);

//Parte ricorsiva della funzione print_preord_rbt
void rbtPrintPrec(rbt_t T);

//Trova il nodo del RBT che contiene la chiave, se esiste
rbt_node_t *rbtFind(rbt_t T, int n);