#pragma once

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define RANDOM_ANYSEED ~0ULL

void randomSeed(uint64_t x);

float randomF(float low, float upp);
double randomD(double low, double upp);
uint32_t random32(uint32_t low, uint32_t upp);
uint64_t random64(uint64_t low, uint64_t upp);

#ifdef __cplusplus
}
#endif
