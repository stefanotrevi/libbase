#pragma once

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

// Returns 1 if the file exists, 0 otherwise
int fileExists(const char *fname);

// Portable function to map memory
void *fileMap(const char *fname, size_t size, int readonly);

// Portable function to unmap memory
void fileUnmap(void *map, size_t size);

// Portable function to copy files efficiently
size_t fileCopy(const char *dst_name, const char *src_name);

// Portable function to create a directory
int fileMkdir(const char *dname);

// Tells size of file
size_t fileSize(const char *fname);

#ifdef __cplusplus
}
#endif
