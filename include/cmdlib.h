#include <stdio.h>
#include "matrixlib.h"
#include "commonlib.h"

#define VARL 1000
#define ERR -1

typedef struct
{
    char var[MAXL];
    char funct[MAXL];
    char arg[MAXL][MAXL];
    char print;
} command;

extern Matrice vars[VARL];
extern int v_dim;

//Funzione principale, si occupa di eseguire la shell
void cmdShell(void);

//Elimina gli spazi e un carattere da una stringa, poi sposta il puntatore a dove si è arrivati
void spostaStringa(char **str, int q);

//Legge il comando da terminale e lo divide nei vari componenti (variabile, funzione e argomenti)
command readCmd(void);

//Prende in input un comando già elaborato, ed esegue la funzione corrispondente, eventualmente genera delle eccezioni
void selectCmd(command cmd);

//Applica "cercaVar" ad un comando, estraendo tutte le variabil. Ritorna 0 in caso di errore, 1 altrimenti
int estraiVars(command cmd, int *v, int is_void);

//Cerca un elemento nell'array globale. Se assente, e is_new è 1, crea un elemento nuovo. Altrimenti ritorna -1
int cercaVar(char var[MAXL], int is_new);

//Inserisce tutti gli operatori trovati nel comando in una stringa (+, -, *, /)
char cercaOperatore(char *input);

//Prende in input il numero dell'eccezione e stampa un messaggio di errore, ritorna sempre 0
int cmdExceptions(int n);

//Copia parte del contenuto di input in out, fintanto che non trova op o ' ', poi sposta avanti l'input
char elaboraInput(char *out, char **input, char op);
