#pragma once

//Nodo di un BST
typedef struct bst_node
{
    struct bst_node *father;
    struct bst_node *left;
    struct bst_node *right;
    int key;
} bst_node_t;

//Albero binario di ricerca (BST)
typedef struct bst
{
    bst_node_t *root;
    int numel;
} bst_t;
