#pragma once

//Nodo di un albero Red-black
typedef struct rbt_node
{
    struct rbt_node *father;
    struct rbt_node *left;
    struct rbt_node *right;
    int key;
    int color;
} rbt_node_t;

//Albero Red-black
typedef struct rbt
{
    rbt_node_t *root;
    int numel;
} rbt_t;
