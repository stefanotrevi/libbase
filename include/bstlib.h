#pragma once

#include "bsttypes.h"

#define BST_ERROR 1
#define BST_SUCCESS 0

//Inizializza un BST vuoto
void bstInit(bst_t *t);

//Elimina un BST
void bstDelete(bst_t *T);

//Funzione ricorsiva per eliminare un sotto-albero
void bstDeleteNodes(bst_node_t *n);

//Inserisce un nuovo elemento nel BST
bst_t *bstPut(bst_t *T, int key);

//Rimuove un elemento dall'albero binario
int bstRemove(bst_t *T, bst_node_t *n);

//Trapianta un sotto-albero in un altro punto dell'albero
bst_t *bstPlant(bst_t *T, bst_node_t *n, bst_node_t *m);

//Stampa un BST in ordine crescente (in senso lato)
bst_t bstPrintOrd(bst_t T);

//Parte ricorsiva della funzione print_order_BST
void bstPrintOrec(bst_t T);

//Stampa un BST in modo pre-order (padre-sinistro-destro)
bst_t bstPrintPre(bst_t T);

//Parte ricorsiva della funzione print_preorder_BST
void bstPrintPrerec(bst_t T);

//Ritorna il nodo che contiene n, se esiste, oppure NULL
bst_node_t *bstFind(bst_t T, int n);

//Ritorna il nodo contenente il minimo, se l'albero non è vuoto, oppure NIL
bst_node_t *bstMin(bst_t T);

//Ritorna il nodo contenente il massimo, se l'albero non è vuoto, oppure NIL
bst_node_t *bstMax(bst_t T);

//Ritorna il nodo successivo ad n, ammesso che n non contenga il massimo
bst_node_t *bstNext(bst_node_t *n);

//Ritorna il nodo precedente ad n, ammesso che n non contenga il minimo
bst_node_t *bstPrec(bst_node_t *n);
