//In questa libreria sono contenute diverse funzioni utili per svolgere conti con le matrici, sviluppate da me.
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commonlib.h"
#include "listlib.h"

//Definizione del tipo "Matrice", con 3 campi: la matrice degli elementi, il numero di righe e il numero di colonne
typedef struct
{
    double **elemento;
    char nome[MAXL];
    int righe;
    int colonne;
    char temp;
} Matrice;

//Mostra una breve introduzione al programma
void intro(void);

//Inizializza una matrice nulla
void initVoidMatrix(Matrice *M);

//Salva una matrice, dovrà essere eliminata manualmente dalla memoria
void saveMatrix(Matrice *M, char *nome);

//Elimina una matrice dalla memoria
void delMatrix(Matrice *M);

//Inserisce in tutti gli elementi di M il valore "f", solo nei diagonali se la flag è vera (mette a 0 gli altri)
void fToMatrix(double f, Matrice *M, int diagonale);

//Inserisce una matrice "grezza" di double nella matrice M, troncandola se supera la dimensione di M
void fVToMatrix(double **f, int fRighe, int fColonne, Matrice *M);

//Inizializza una matrice righe*colonne, con gli elementi = "valore" (solo diagonali se flag = 1, gli altri = 0)
void initMatrix(Matrice *M, char *nome, int righe, int colonne, double valore, int diagonale);

//Estrae M da una lista, per righe, usando la sintassi da console
void listMatrix(Matrice *M, Lista L);

//Estrame M da una lista, per righe, usando la sintassi da file
void fListMatrix(Matrice *M, Lista L);

//Estrae M da input di console, di default per righe. Separatore colonne: [SPAZIO], separatore righe [,], trasposta ['].
void getMatrix(Matrice *M);

//Estrae M da una stringa, di default per righe. Separatore colonne: [SPAZIO], separatore righe [,], trasposta ['].
void getStringMatrix(char *str, Matrice *M);

//Estrae M da un file di testo, per righe. Separatore colonne: [SPAZIO], separatore righe: [INVIO], separatore matrici: [;], trasposta ['].
void getFileMatrix(FILE *fT, Matrice *M);

//Copia la matrice M1 nella matrice M2
void copiaMatrice(Matrice M1, Matrice *M2);

//Stampa M a video
void stampaMatrice(Matrice M);

//Ritorna la traccia (somma elementi diagonali) di M, ritorna 0 se M non è quadrata
double traccia(Matrice M);

//Ritorna la matrice trasposta di M
Matrice trasposta(Matrice M);

//Ritorna la somma fra due matrici. Se incompatibili, stampa un messaggio di errore e ritorna la matrice vuota
Matrice somma(Matrice M1, Matrice M2);

//Ritorna il prodotto fra due matrici, se non sono compatibili, stampa un messaggio di errore e ritorna la matrice vuota
Matrice prodotto(Matrice M1, Matrice M2);

//Ritorna il prodotto fra una matrice e uno scalare
Matrice prodottoSc(Matrice M, double f);

/*Ritorna la sottomatrice di M rispetto alla riga e alla colonna inserite
Se è impossibile ricavare una sottomatrice, ritorna una matrice nulla*/
Matrice sottoMatrice(Matrice M, int riga, int colonna);

//Ritorna il determinante della matrice inserita. Se non è quadrata, stampa un errore
double det(Matrice M);

//Ritorna la matrice aggiunta di M. Se non è quadrata, stampa un errore
Matrice adj(Matrice M);

//Ritorna l'inversa di M. Se il determinante è nullo, ritorna la marice vuota
Matrice inversa(Matrice M);

/*
//Ritorna la matrice associata rispetto a B e B1, data una matrice rispetto a cui associare una funzione rispetto alle basi canoniche
Matrice cambiaBaseC(Matrice A, Matrice B, Matrice B1);
*/

//Ritorna la matrice associata rispetto a B e B1, data la matrice associata rispetto alle basi canoniche
Matrice cambiaBaseC(Matrice A, Matrice B, Matrice B1);

//Ritorna la matrice associata rispetto a D e D1, data la matrice associata rispetto alle basi B e B1
Matrice cambiaBase(Matrice A, Matrice B, Matrice B1, Matrice D, Matrice D1);

//Elimina la riga/colonna (1/0) desiderata dalla matrice (si conta partendo da 0). Ritorna 1 se la matrice è quadrata, 0 altrimenti
int riduciRC(Matrice *M, int rc, int select);

/*Aggiorna gli elementi di un vettore di incrocio, se il primo elemento è negativo, aumenta tutto di uno,
altrimenti lo aggiorna tenendo conto del limite di righe/colonne*/
int aggiornaV(int *v, int dim, int limit);

//Ritorna la matrice risultante dall'incrocio di un vettore di righe con un vettore di colonne. Un qualsiasi valore negativo conta come terminatore
Matrice incrocio(Matrice M, int *righe, int *colonne);

//Ritorna il rango della matrice
int rango(Matrice M);

/*Algoritmo di gauss per il determinante. Il block serve per non modificare la matrice originale,
1 se non si vuole modificare la matrice di input, 0 per risparmiare tempo/modificare la matrice*/
double gauss(Matrice M, int block);

//Variante dell'algoritmo di Gauss, implementato per trovare il rango. Il block ha lo stesso scopo
int rGauss(Matrice M, int block);

//Ritorna 1 se la matrice è simmetrica, 0 altrimenti
int simmetrica(Matrice M);

//Genera un vettore con i coefficienti della forma quadratica associata alla matrice
double *fQ(Matrice M, int *dim);

//Stampa la forma quadratica partendo dal vettore dei coefficienti (ordinati secondo il calcolo fatto da "fQ")
void stampaFQ(Matrice M, double *vC);

//Ridimensiona la matrice M. Se le dim. non sono compatibili, verrà eseguito il reshape con eccezioni.
Matrice reshape(Matrice M, int righe, int colonne);

//Scambia le righe r1 ed r2 della matrice. Nota: scambiare le righe è molto più veloce che scambiare le colonne
void scambiaRighe(Matrice *M, int r1, int r2);

//Scambia le colonne r1 ed r2 della matrice. Nota: scambiare le righe è molto più veloce che scambiare le colonne
void scambiaColonne(Matrice *M, int r1, int r2);

//Ritorna la matrice (vettore) delle incognite di un sistema di equazioni lineari
Matrice sistema(Matrice A, Matrice b);