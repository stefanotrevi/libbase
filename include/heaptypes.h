#pragma once

//Heap basato su array
typedef struct heap
{
    int *element;
    int height;
    size_t size;
    size_t max_size;
} heap_t;
