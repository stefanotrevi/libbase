
//LIBRERIA DEPRECATA!!!
/* in questa libreria sono contenute diverse funzioni create da me durante il corso di programmazione,
che si rivelano utili di volta in volta */
#include "supportlib.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#ifdef _WIN32
#include <windows.h>
#endif
#ifdef __unix__
#define CLEAR "clear"
#elif _WIN32
#define CLEAR "cls"
#else
#endif
#ifdef __INTEL_COMPILER
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif

//lunghezza massima per le stringhe
#define MAXL 100
//lunghezza massima per delle matrici, o vettori corti
#define MAXD 110
//lunghezza massima per dei vettori lunghi
#define MAXV 1000

//genera "n" numeri interi casuali fra il minimo e il massimo, inclusi, inserendoli nell'array "out")
void RNG(int *out, int min, int max, int n, int seed);

//genera "n" numeri float casuali fra il minimo e il massimo, inclusi, inserendoli nell'array "out")
void fRNG(float *out, float min, float max, int n, int seed);

//Funzione per spostare a destra tutti gli elementi di un vettore,da "pos"
void slittaVettore(int *v, int dim, int pos);

//implementazione dell'InsertionSort su un vettore "v" di dimensione "dim"
void ordinaReplace(int *v, int dim);

//implementazione dell'algoritmo di Quicksort, applicabile ad un vettore di interi
// I parametri sono: vettore da ordinare, dimensione del vettore
void ordinaRicorsivo(int *v, int dim);

//Ordina un vettore di interi conoscendo il minimo e il massimo. Se sconosciuti (entrambi = 0), li trova
void countSort(int *v, int dim, int min, int max);

// stampa un array di interi di dimensione "dim"
void printArray(int *v, int dim);

//Copia un array in un altro
void copiaArray(int *dest, int *src, int dim);
// stampa a console la fattorizzazione di un numero
void fattorizzazione(int a);

// calcola la potenza di interi
int potenza(int base, int esp);

// inverte le cifre di un numero senza usare array
int numeroContrario(int a);

/*scansiona "n" elementi inserendoli nel vettore "buff". Potrebbe essere necessario
modificare la funzione a seconda di come si voglia effettuare la scansione, ad es.
se si conosce o meno a priori il numero di elementi da inserire*/
void nscanf(int *buff, int n);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file con nome in *argv[] esistano, eventualmente stampando il primo che non esiste
void eccezioneFile(FILE **f, int nFile, char **argv);

//apre un file con il nome immesso come parametro. Se vuoto, chiede il nome all'utente
FILE *openFile(char *nome, char *modo);

//Copia f1 in f2
void copiaFile(FILE *f1, FILE *f2);

//Copia una matrice contenuta in f1 nel file temporaneo f2, per permetterne la lettura
FILE *copiaFileMatrice(FILE *f1, FILE *f2, char fName[MAXL]);

//Stampa un file di testo a console
void stampaFC(FILE *fT);

//Stampa un file di testo in un altro file di testo
void stampaFF(FILE *f1, FILE *f2);

//calcola la radice di un quadrato perfetto
int pradq(int n);

//cambia il segno di un numero = 1 o -1. Utile per sommare un elemento a segno variabile
void cambiaSegno(int *segno);

//ritorna il maggiore fra i due, "a" se sono uguali
int intMax(int a, int b);

//ritorna il minore fra i due, "a" se sono uguali
int intMin(int a, int b);

//Inizializza tuti i valori di un array al valore desiderato
void initArray(int *v, int dim, int valore);

//Inizializza una matrice di double allocando righe*colonne celle di memoria
void fmMalloc(double ***f, unsigned int righe, unsigned int colonne);

//Inizializza una matrice di interi con segno allocando righe*colonne celle di memoria
void intmMalloc(int ***n, unsigned int righe, unsigned int colonne);

//Inizializza una matrice di interi senza segno allocando righe*colonne celle di memoria
void uIntmMalloc(unsigned int ***n, unsigned int righe, unsigned int colonne);

//Inizializza un array di stringhe e inserisce "in" al suo interno
void mStrMalloc(char ***s, char *in, int size);

//Libera la memoria occupata da una matrice
void mFree(void **ptr, int righe);

//Cerca un intero all'interno di un array. Ritorna 1 se trovato, 0 altrimenti
int cercaIntero(int n, int *v, int dim);

//Cerca un carattere all'interno di una stringa. Ritorna 1 se trovato, 0 altrimenti
int cercaChar(char *s, char c);

//Stampa un carattere speciale usando la tabella escape
void stampaSpeciale(char *in);

//Stampa le cifre di un numero, formattate in modo speciale, (usato per i pedici)
void stampaCifreSp(char **s, int c, int nCifre);

//Aumenta di uno l'ennesimo carattere della prima stringa dell'array, se non riesce, prova con la successiva. Ritorna un controllo
//In pratica si tratta di aumentare le cifre in una base (es. in base 10: 8 + 1 = 9, 9 + 1 N.A. => aggiungo una cifra)
//(usato per la stampa di pedici > di 9)
int aggiornaMStr(char **in, int c, int dim, char limit);

//Una versione di fflush che funziona anche su Linux
void flusher(void);

//Una procedura di uscita che richiede la pressione di un tasto
void quit(void);

//Una procedura di attesa, si può scegliere se stampare dei puntini per visualizzare l'attesa
clock_t active_sleep(int ms, int print);

//Stampa un file di aiuto
void help(void);

//Funzione per misurare il tempo con maggiore precisione in sistemi Windows
#ifdef _WIN32
__int64 WinCrono(void);
#endif