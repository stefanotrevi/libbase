// Questo header crea un overload
#ifndef TPL_FNAME
    #error Function name is missing: define TPL_FNAME=function_name
#endif
#if !defined(TPL_PROTOTYPE) && !defined(TPL_BODY)
    #error Function body is missing: define TPL_BODY={function_body}, or define TPL_PROTOTYPE \
    if you want a function declaration
#endif

// Controlliamo se abbiamo almeno i tipi o i nomi
#if !defined(TPL_RT) && !defined(TPL_RN)
    #error Missing return type!
#endif
#if defined(TPL_A1) && !defined(TPL_T1) && !defined(TPL_N1)
    #error Missing 1st type
#endif
#if defined(TPL_A2) && !defined(TPL_T2) && !defined(TPL_N2)
    #error Missing 2nd type
#endif
#if defined(TPL_A3) && !defined(TPL_T3) && !defined(TPL_N3)
    #error Missing 3rd type
#endif
#if defined(TPL_A4) && !defined(TPL_T4) && !defined(TPL_N4)
    #error Missing 4th type
#endif

#include "tpl_def_typenames.h" // Prova a dedurre i tipi dai nomi o viceversa

#if defined(TPL_A4)
    #define TPL_ARGS TPL_T1 TPL_A1, TPL_T2 TPL_A2, TPL_T3 TPL_A3, TPL_T4 TPL_A4
    #define TPL_FN TPL_FN4(TPL_RN, TPL_FNAME, TPL_N1, TPL_N2, TPL_N3, TPL_N4)
#elif defined(TPL_A3)
    #define TPL_ARGS TPL_T1 TPL_A1, TPL_T2 TPL_A2, TPL_T3 TPL_A3
    #define TPL_FN TPL_FN3(TPL_RN, TPL_FNAME, TPL_N1, TPL_N2, TPL_N3)
#elif defined(TPL_A2)
    #define TPL_ARGS TPL_T1 TPL_A1, TPL_T2 TPL_A2
    #define TPL_FN TPL_FN2(TPL_RN, TPL_FNAME, TPL_N1, TPL_N2)
#elif defined(TPL_A1)
    #define TPL_ARGS TPL_T1 TPL_A1
    #define TPL_FN TPL_FN1(TPL_RN, TPL_FNAME, TPL_N1)
#else
    #define TPL_ARGS void
    #define TPL_FN TPL_FN0(TPL_RN, TPL_FNAME)
#endif

#ifndef TPL_ATR // non è definito nessun attributo (e.g. static, inline), lasciamo vuoto
    #define TPL_ATR
    #define TPL_UNDEF_ATR // dobbiamo assicurare che solo se lo definiamo qui viene cancellato
#endif
#ifndef TPL_PTR // non è definito nessun puntatore, lasciamo vuoto
    #define TPL_PTR
    #define TPL_UNDEF_PTR
#endif

// Dichiarazione o definizione
#ifdef TPL_PROTOTYPE
TPL_ATR TPL_RT TPL_PTR TPL_FN(TPL_ARGS);
#else
TPL_ATR TPL_RT TPL_PTR TPL_FN(TPL_ARGS)
    TPL_BODY
#endif

// Puliamo tutte le macro specifiche dell'istanza corrente
#ifdef TPL_UNDEF_ATR
    #undef TPL_ATR
    #undef TPL_UNDEF_ATR
#endif
#ifdef TPL_UNDEF_PTR
    #undef TPL_PTR
    #undef TPL_UNDEF_PTR
#endif
#undef TPL_ARGS
#undef TPL_FOO
#undef TPL_RT
#undef TPL_RN
#undef TPL_T1
#undef TPL_T2
#undef TPL_T3
#undef TPL_T4
#undef TPL_N1
#undef TPL_N2
#undef TPL_N3
#undef TPL_N4
