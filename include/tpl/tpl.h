/** tpl.h
 * Gli header tpl (template) permettono di creare, in modo relativamente semplice, dichiarazioni e 
 * definizioni di funzioni con tipo generico usando solo il linguaggio C, attraverso l'uso di macro 
 * e direttive del preprocessore. 
 * LIMITAZIONI: 
 * 1) Il numero massimo di possibili argomenti è 4, (del tutto arbitrario)
 * 2) Difficoltà nell'individuare eventuali errori, poiché il compilatore non restituirà il numero 
 *  di linea corretto, (si consiglia vivamente, prima di tutto, di implementare il codice con un tipo 
 *  specifico)
 * 3) E' possibile fare (automaticamente) l'overload del tipo solo se il livello di reindirizzamento
 *  è lo stesso (e.g. int** -> float** OK!, ma int** -> int KO!), poiché per creare variabili di 
 *  tipo dereferenziato, è necessario che il tipo sia sempre quello primitivo, e l'operatore di
 *  indirizzo sia affiancato al parametro formale, e non al tipo.
 * 4) E' comunque necessario costruire ogni funzione esplicitamente (e non "on-demand", rilevando 
 *  i tipi dei parametri, poiché i tipi "non esistono" per il preprocessore)
 * 5) La creazione di una funzione generica è possibile in C11 attraverso _Generic, e deve essere
 *  fatto a mano.
 * 
 * FUNZIONAMENTO:
 * Supponiamo di voler creare una piccola libreria di funzioni, avremo un file sorgente ed un 
 * header:
 * 1) Nel file sorgente, includere l'header.
 * 2) Nell'header, includere "tpl.h"
 * 
 * Iniziamo ora a creare un prototipo nell'header. Per prima cosa, dobbiamo creare delle macro 
 * comuni a tutti gli overload ([] significa "opzionale"):
 * 3) TPL_PROTOTYPE <nulla o qualsiasi valore>
 * 4) TPL_FNAME <nome della funzione>
 * 5) [TPL_ATR] <lista di attributi funzione> (e.g. static, inline...)
 * 6) [TPL_PTR] <zero o più asterischi> (la funzione ritorna un puntatore)
 * 7) [TPL_A1,2,3,4] <nome argomenti 1,2,3,4> (il nome può essere preceduto da asterischi)
 * 
 * Ora che abbiamo i parametri comuni, definiamo le macro dei tipi:
 * 8) TPL_RT <tipo di ritorno> (anche void)
 * 9) [TPL_T1,2,3,4] <tipo argomenti 1,2,3,4> (anche void, se sono puntatori)
 * 10) includere l'header "tpl_build.h"
 *
 * Ripetere a piacere i passi 8-10, ogni volta verrà generato il prototipo di una nuova funzione in 
 * base a TPL_FNAME e ai tipi di ritorno/argomenti (variabili, tenendo conto della limitazione 3).
 * Non serve annullare la definizione delle macro vecchie, se ne occupa già "tpl_build".
 * 11) Includere l'header "tpl_finish.h", che si occupa di "pulire" l'ambiente.
 * 
 * Se si vogliono dichiarare altre funzioni, basta ripetere i passi 3-11. Abbiamo quindi completato 
 * la costruzione dell'header. Ora, nel file sorgente, bisogna ripetere più o meno gli stessi 
 * passaggi, con le seguenti differenze:
 * 1) Non definire TPL_PROTOTYPE (abbiamo già il prototipo)
 * 2) Dopo aver definito i nomi degli argomenti (punto 7), definire:
 *  TPL_BODY <corpo_della_funzione>
 * 
 * TPL_BODY contiene tutto il corpo della funzione, incluse le parentesi graffe. Al suo interno,
 * è possibile usare le macro TPL_T1,2,3,4 come place-holder per i tipi di eventuali variabili
 * temporanee, e i nomi degli argomenti sono quelli forniti per TPL_A1,2,3,4 (il loro contenuto!).
 * 
 * Poi, si procede come nell'header, e a questo punto la funzione è pronta per essere usata, e per 
 * averne una con un nuovo tipo, basterà ripetere i passi 8-10.
 * 
 * VARIAZIONI:
 * 1) Se la funzione è ricorsiva, le chiamate ricorsive devono essere sotituite con la macro 
 *  TPL_RECURSE(<argomenti>), che verrà opportunamente espansa da tpl_build. 
 * 2) Se è necessario chiamare altre funzioni, in base al tipo, è possibile sfruttare il costrutto
 *  _Generic() di C11. Questo costrutto è caldamente consigliato anche alla fine dei prototipi di 
 *  tutti gli "overload", in modo da avere una sola funzione da chiamare (magari identificata dal 
 *  "nome base"), che si adatta in base ai tipi. Purtroppo, poiché _Generic deve essere associato 
 *  ad una macro, non è possibile automatizzare questo processo. 
 * 3) Se il tipo desiderato ha spazi nel nome (e.g. unsigned char), è necessario fornire, assieme 
 *  al tipo, anche un acronimo, attraverso le macro TPL_N1,2,3,4. Per comodità, attraverso un 
 *  sistema di deduzione dei tipi, se queste macro sono definite come certe particolari costanti 
 *  multi-carattere, non serve dire esplicitamente il tipo. Nota che questa deduzione si basa sul 
 *  confronto fra caratteri multi-byte, che è implementation-defined (vedi ISO C 2011 6.4.4.4), 
 *  quindi per avere portabilità bisogna per forza definire sempre sia il tipo che l'acronimo.
 *  I tipi riconosciuti dagli acronimi sono:
 *      'uc' -> unsigned char     'us' -> unsigned short    'ui' -> unsigned int      
 *      'ul' -> unsigned long     'll' -> long long         'ull'-> unsigned long long
 *      'ld' -> long double
 *  Per gli struct o gli enum, o si esegue prima un typedef oppure bisogna sempre fornire tipo E 
 *  acronimo.
*/
#pragma once

#define TPL_MAX_ARG 4    // massimo numero di argomenti
#define TPL_MAX_OVLOAD 4 // massimo numero di overload

#define TPL_RECURSE(...) TPL_FN(__VA_ARGS__) // usare per le chiamate ricorsive

#define TPL_CAT2(x, y) x##y
#define TPL_CAT(x, y) TPL_CAT2(x, y)
#define TPL_NCAT(x, y) TPL_CAT(x, TPL_CAT(_, y))
#define TPL_EXPAND(x) TPL_CAT(x, )

// Queste macro sono comode per semplificare il codice in tpl_head
#define TPL_FN0(rt, fn) TPL_NCAT(rt, fn)
#define TPL_FN1(rt, fn, t1) TPL_NCAT(TPL_FN0(rt, fn), t1)
#define TPL_FN2(rt, fn, t1, t2) TPL_NCAT(TPL_FN1(rt, fn, t1), t2)
#define TPL_FN3(rt, fn, t1, t2, t3) TPL_NCAT(TPL_FN2(rt, fn, t1, t2), t3)
#define TPL_FN4(rt, fn, t1, t2, t3, t4) TPL_NCAT(TPL_FN3(rt, fn, t1, t2, t3), t4)
