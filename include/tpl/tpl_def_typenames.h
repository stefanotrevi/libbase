// I confronti con caratteri multi-byte sono implementation-defined (ISO C11 6.4.4.4), ma nella
// maggioranza dei casi, si traducono in un intero che rappresenta i caratteri accostati
#pragma warning( once : 1899 )
// Se sono stati definiti dei nomi ma non dei tipi, proviamo a dedurre
#if defined(TPL_RT) && !defined(TPL_RN)
    #define TPL_RN TPL_RT
#elif defined(TPL_RN) && !defined(TPL_RT)
    #if TPL_RN == 'uc'
        #undef TPL_RN
        #define TPL_RN uchar
        #define TPL_RT unsigned char
    #elif TPL_RN == 'us'
        #undef TPL_RN
        #define TPL_RN ushort
        #define TPL_RT unsigned short
    #elif TPL_RN == 'ui'
        #undef TPL_RN
        #define TPL_RN uint
        #define TPL_RT unsigned int
    #elif TPL_RN == 'ul'
        #undef TPL_RN
        #define TPL_RN ulong
        #define TPL_RT unsigned long
    #elif TPL_RN == 'll'
        #undef TPL_RN
        #define TPL_RN ll
        #define TPL_RT long long
    #elif TPL_RN == 'ull'
        #undef TPL_RN
        #define TPL_RN ullong
        #define TPL_RT unsigned long long
    #elif TPL_RN == 'ld'
        #undef TPL_RN
        #define TPL_RN ldouble
        #define TPL_RT long double
    #else // altrimenti il tipo diventa il nome (che NON deve essere un carattere)
        #define TPL_RT TPL_RN
    #endif
#endif

#if defined(TPL_T1) && !defined(TPL_N1)
#define TPL_N1 TPL_T1
#elif defined(TPL_N1) && !defined(TPL_T1)
    #if TPL_N1 == 'uc'
        #undef TPL_N1
        #define TPL_N1 uchar
        #define TPL_T1 unsigned char
    #elif TPL_N1 == 'us'
        #undef TPL_N1
        #define TPL_N1 ushort
        #define TPL_T1 unsigned short
    #elif TPL_N1 == 'ui'
        #undef TPL_N1
        #define TPL_N1 uint
        #define TPL_T1 unsigned int
    #elif TPL_N1 == 'ul'
        #undef TPL_N1
        #define TPL_N1 ulong
        #define TPL_T1 unsigned long
    #elif TPL_N1 == 'll'
        #undef TPL_N1
        #define TPL_N1 ll
        #define TPL_T1 long long
    #elif TPL_N1 == 'ull'
        #undef TPL_N1
        #define TPL_N1 ullong
        #define TPL_T1 unsigned long long
    #elif TPL_N1 == 'ld'
        #undef TPL_N1
        #define TPL_N1 ldouble
        #define TPL_T1 long double
    #else // altrimenti il tipo diventa il nome (che NON deve essere un carattere)
        #define TPL_T1 TPL_N1
    #endif
#endif

// Ripetiamo la stessa cosa per tutti i tipi/nomi
#if defined(TPL_T2) && !defined(TPL_N2)
#define TPL_N2 TPL_T2
#elif defined(TPL_N2) && !defined(TPL_T2)
    #if TPL_N2 == 'uc'
        #undef TPL_N2
        #define TPL_N2 uchar
        #define TPL_T2 unsigned char
    #elif TPL_N2 == 'us'
        #undef TPL_N2
        #define TPL_N2 ushort
        #define TPL_T2 unsigned short
    #elif TPL_N2 == 'ui'
        #undef TPL_N2
        #define TPL_N2 uint
        #define TPL_T2 unsigned int
    #elif TPL_N2 == 'ul'
        #undef TPL_N2
        #define TPL_N2 ulong
        #define TPL_T2 unsigned long
    #elif TPL_N2 == 'll'
        #undef TPL_N2
        #define TPL_N2 ll
        #define TPL_T2 long long
    #elif TPL_N2 == 'ull'
        #undef TPL_N2
        #define TPL_N2 ullong
        #define TPL_T2 unsigned long long
    #elif TPL_N2 == 'ld'
        #undef TPL_N2
        #define TPL_N2 ldouble
        #define TPL_T2 long double
    #else
        #define TPL_T2 TPL_N2
    #endif
#endif

#if defined(TPL_T3) && !defined(TPL_N3)
#define TPL_N3 TPL_T3
#elif defined(TPL_N3) && !defined(TPL_T3)
    #if TPL_N3 == 'uc'
        #undef TPL_N3
        #define TPL_N3 uchar
        #define TPL_T3 unsigned char
    #elif TPL_N3 == 'us'
        #undef TPL_N3
        #define TPL_N3 ushort
        #define TPL_T3 unsigned short
    #elif TPL_N3 == 'ui'
        #undef TPL_N3
        #define TPL_N3 uint
        #define TPL_T3 unsigned int
    #elif TPL_N3 == 'ul'
        #undef TPL_N3
        #define TPL_N3 ulong
        #define TPL_T3 unsigned long
    #elif TPL_N3 == 'll'
        #undef TPL_N3
        #define TPL_N3 ll
        #define TPL_T3 long long
    #elif TPL_N3 == 'ull'
        #undef TPL_N3
        #define TPL_N3 ullong
        #define TPL_T3 unsigned long long
    #elif TPL_N3 == 'ld'
        #undef TPL_N3
        #define TPL_N3 ldouble
        #define TPL_T3 long double
    #else
        #define TPL_T3 TPL_N3
    #endif
#endif

#if defined(TPL_T4) && !defined(TPL_N4)
#define TPL_N4 TPL_T4
#elif defined(TPL_N4) && !defined(TPL_T4)
    #if TPL_N4 == 'uc'
        #undef TPL_N4
        #define TPL_N4 uchar
        #define TPL_T4 unsigned char
    #elif TPL_N4 == 'us'
        #undef TPL_N4
        #define TPL_N4 ushort
        #define TPL_T4 unsigned short
    #elif TPL_N4 == 'ui'
        #undef TPL_N4
        #define TPL_N4 uint
        #define TPL_T4 unsigned int
    #elif TPL_N4 == 'ul'
        #undef TPL_N4
        #define TPL_N4 ulong
        #define TPL_T4 unsigned long
    #elif TPL_N4 == 'll'
        #undef TPL_N4
        #define TPL_N4 ll
        #define TPL_T4 long long
    #elif TPL_N4 == 'ull'
        #undef TPL_N4
        #define TPL_N4 ullong
        #define TPL_T4 unsigned long long
    #elif TPL_N4 == 'ld'
        #undef TPL_N4
        #define TPL_N4 ldouble
        #define TPL_T4 long double
    #else 
        #define TPL_T4 TPL_N4
    #endif
#endif
