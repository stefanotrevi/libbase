/* Qui sono contenute funzioni non pensate per essere usate "così come sono",
ma servono alle funzioni in "commonlib" */
#include <stdio.h>
#include <stdlib.h>

#define RED "\x1b[91m"
#define GREEN "\x1b[92m"
#define YELLOW "\x1b[93m"
#define BLUE "\x1b[94m"
#define MAGENTA "\x1b[95m"
#define CYAN "\x1b[96m"
#define RESET "\x1b[0m"

#define PEDEX0 "\u2080"
#define PEDEX1 "\u2081"
#define PEDEX2 "\u2082"
#define PEDEX3 "\u2083"
#define PEDEX4 "\u2084"
#define PEDEX5 "\u2085"
#define PEDEX6 "\u2086"
#define PEDEX7 "\u2087"
#define PEDEX8 "\u2088"
#define PEDEX9 "\u2089"
#define SQUARE "\u00b2"

//Funzione usata per il QuickSort
int checkArray(int *v, int dim);

//Funzione usata per il QuickSort
int calcolaDim(int *v, int dim, int *average);

//Funzione usata per il QuickSort
void riempiPartialArray(int *v, int dim, int ndim1, int average);

//Usato per calcolare il numero contrario
int calcolaCifre(int n);

//Usato per stampare i caratteri speciali a console
int tavolaEscape(char *in);

//Usato per stampare i caratteri speciali a file
int tavolaEscapeFile(char *in, FILE *fT);

//Inizializza un array con ogni elemento pari al valore del precedente più uno, a partire da "vI". Usato per il rango
void initArrayAdd(int *v, int dim, int vI);

//Procedura usata in Reshape
void reshapeMatrice(double **M1, double **M2, int righe1, int colonne1, int righe2, int colonne2);