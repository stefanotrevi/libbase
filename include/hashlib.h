#pragma once

#include "hashtypes.h"

//Numeri primi nel file primes.txt, al momento 10 MLN (dim. massima hash table ~= 2^27)
#define NPRIME 10000000
#define HASH_ERR_SIZE 1
#define HASH_ERR_KIND 2

//Elimina la mappatura in memoria del file primes.dat
void freePrimes(void);

/* Crea una hash table con chaining, specificando una dimensione e il numero di ripetizioni 
* accettate, usando per il chaining una struttura in hashkind_t */
int hcInit(hctable_t *ht, int dim, int repeats, hashkind_t kind);

//Ritorna il numero primo più adatto per la funzione hash
int findPrime(int dim);

//Funzione di hash, ritorna l'indice in cui inserire il dato n
int fhash(int mod, int n);

//Inserisce l'elemento nella hash table con chaining
hctable_t *hcPut(hctable_t *ht, int n);

//Elimina una hash table con chaining
void hcDelete(hctable_t *ht);

//Stampa una hash table con chaining
void hcPrint(hctable_t ht);

//Cerca un elemento nella hash table con chaining
void *hcFind(hctable_t ht, int n);

//Inizializza una hash table (con open addressing) di dimensione dim
void hoInit(hotable_t *ht, int dim, hashkind_t kind);

//Funzione di hash per le tabelle con open address
int ofhash(int mod, int n, int i);

//Inserisce l'elemento nella hash table con open addressing, restituisce eventualmente un errore
int hoPut(hotable_t *ht, void *d);

//Elimina una hash table con open addressing
void hoDelete(hotable_t *ht);

//Stampa una hash table con open addressing
void hoPrint(hotable_t ht);

//Cerca un elemento nella hash table con open addressing
void *hoFind(hotable_t ht, int n);
