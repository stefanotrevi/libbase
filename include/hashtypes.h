#pragma once

#include "bsttypes.h"
#include "rbttypes.h"
#include "settypes.h"
#include "listtypes.h"

// tipi di tabella hash
typedef enum hashkind
{
    HASH_LIST,  // (hctable) con lista (semplice ma lento con molte ripetizioni)
    HASH_BST,   // (hctable) con BST (un buon compromesso, fra semplicità e prestazioni)
    HASH_RBT,   // (hctable) con RBT (prestazioni migliori, non si può eliminare un singolo elemento)
    OHASH_SET,  // (hotable) contiene Set.
    OHASH_RSET, // (hotable) contiene Rset.
} hashkind_t;

typedef void *chain_t;

//Tabella hash con Chaining
typedef struct hctable
{
    chain_t *table;
    int size;
    void (*init)(chain_t table);
    void (*del)(chain_t table);
    chain_t (*put)(chain_t table, int n);
    chain_t (*find)(chain_t table, int n);
    chain_t (*print)(chain_t table);
} hctable_t;

//La tabella hash può contenere sia elementi a rango che non

//Elemento della tabella con open addressing
typedef struct ho_element
{
    void *el;
    char busy; //Booleano per sapere se l'elemento è allocato o no
} ho_element_t;

//Tabella con Open Addressing
typedef struct hotable
{
    ho_element_t *table;
    int (*getData)(void *el);
    void* (*getRep)(void *el);
    int size;
    int numel;
} hotable_t;
