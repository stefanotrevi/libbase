#pragma once

#ifdef _WIN32
    #define CLEAR "cls"
    #define INTRIN_H <intrin.h>
#else
    #define CLEAR "clear"
    #define INTRIN_H <x86intrin.h>
#endif

#include <stdio.h>

//Colori
#define RED "\x1b[91m"
#define GREEN "\x1b[92m"
#define YELLOW "\x1b[93m"
#define BLUE "\x1b[94m"
#define MAGENTA "\x1b[95m"
#define CYAN "\x1b[96m"
#define RESET "\x1b[0m"

#define ARRAYOF_TYPE(...) __VA_ARGS__

// clang-format off
#define ARRAY_CAST(x, ...) _Generic((x),                                    \
                    char: (char[])__VA_ARGS__,                              \
                    unsigned char: (unsigned char[])__VA_ARGS__,            \
                    short: (short[])__VA_ARGS__,                            \
                    unsigned short: (unsigned short[])__VA_ARGS__,          \
                    int: (int[])__VA_ARGS__,                                \
                    unsigned int: (unsigned int[])__VA_ARGS__,              \
                    long: (long[])__VA_ARGS__,                              \
                    unsigned long: (unsigned long[])__VA_ARGS__,            \
                    long long: (long long[])__VA_ARGS__,                    \
                    unsigned long long: (unsigned long long[])__VA_ARGS__,  \
                    float: (float[])__VA_ARGS__,                            \
                    double: (double[])__VA_ARGS__,                          \
                    long double: (long double[])__VA_ARGS__)
// clang-format on

// Crea un array anonimo, e ne affianca la lunghezza, partendo da una lista di inizializzazione
#ifdef __ICL
    #define arrayof(x, ...) ARRAYOF_TYPE(ARRAY_CAST(x, {x, __VA_ARGS__}))
#else
    #define arrayof(x, ...) ARRAYOF_TYPE((typeof(x)[]){x, __VA_ARGS__})
#endif

#define ARRAY_PASS(...) __VA_ARGS__, lengthof(__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct int2
{
    int x;
    int y;
} int2_t;

// Sistema gli accenti su Windows
void init(void);

// Sospende il programma fino alla pressione di un tasto
void waitForKey(void);

// Stampa un messaggio di uscita e aspetta un input
void quit(void);

// Ritorna un file aperto in modo sicuro. Se nome è NULL, chiede il nome da stdin
FILE *openFile(char *nome, char *modo);

// Ritorna un vettore casuale, con elementi fra min e max
int *RNG(int *out, int min, int max, size_t numel);

// Stampa tutti gli elementi di un vettore a schermo
int *arrayPrint(const int *v, size_t numel);

// Ritorna l'array con gli elementi spostati a destra di una posizione, partendo da "pos"
int *arraySlideToR(int *v, size_t numel, size_t pos);

// Ritorna 1 se l'array è ordinato in modo crescente, 0 altrimenti
int arrayCheckOrder(int *v, int numel);

// Ritorna il minimo ed il massimo di un array, in quest'ordine
int2_t arrayMinMax(int *v, int numel);

// Versione ottimizzata che use AVX-2
int2_t arrayMinMaxAVX(int *v, int numel);

// Ritorna la potenza di un intero elevato ad un intero
int ipow(int base, int exp);

// Ritorna il logaritmo intero in base due
int ilog2(int n);

// Arrotonda un numero alla successiva potenza di 2
int round2(int n);

// Copia due aree di memoria usando le istruzioni AVX
void memcpyAVX(void *dst_void, const void *src_void, size_t sz);

#ifdef __cplusplus
}
#endif