#pragma once

#include "heaptypes.h"

//Ritorna l'elemento a sinistra di un nodo dell'heap. Ritorna 0 in caso di overflow
int heapLeft(heap_t h, int i);

//Ritorna l'elemento a destra di un nodo dell'heap. Ritorna 0 in caso di overflow
int heapRight(heap_t h, int i);

//Ritorna l'elemento genitore di un nodo dell'heap
int heapParent(heap_t h, int i);

//Trasforma un heap parzialmente "minHeap" in uno completo
void heapMinify(heap_t *h, int i);

//Trasforma un heap qualsiasi in un minHeap
void heapBuildMin(heap_t *h);

//Trasforma un heap parzialmente "maxHeap" in uno completo
void heapMaxify(heap_t *h, int i);

//Versione iterativa di maxHeapify
void heapMaxifyIt(heap_t *h, int i);

//Trasforma un heap qualsiasi in un maxHeap
void heapBuildMax(heap_t *h);

//Trasforma un array in un heap, senza ordinarlo in alcun modo
heap_t arrayToHeap(int *v, size_t dim);

//Implementazione dell'HeapSort
int *heapSort(int *v, size_t dim);