#pragma once

#define NAMELEN 50

//Elemento dell'insieme disgiunto a lista
typedef struct set_el
{
    struct set *set;
    int data;
    struct set_el *next;
} set_el_t;

//Insieme disgiunto a lista
typedef struct set
{
    set_el_t *rep;
    set_el_t *tail;
    unsigned int numel;
} set_t;

//Insieme disgiunto k-ario, rappresenta anche il set stesso
typedef struct rset
{
    struct rset *father;
    int data;
    unsigned int rank;
} rset_t;
