#pragma once

#include "listtypes.h"

// Inizializza una lista vuota
void listInit(list_t *l);

// Elimina una lista
void listDelete(list_t *l);

// Inserisce un nuovo elemento in testa alla lista
list_t *listPut(list_t *l, int dato);

// Stampa tutti gli elementi di una lista
void listPrint(list_t l);

// Ritorna l'indirizzo di n se lo trova, NULL altrimenti
list_node_t *listFind(list_t l, int n);
