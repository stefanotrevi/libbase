#pragma once

//Nodo di una lista doppiamente collegata
typedef struct list_node
{
    int data;
    struct list_node *next;
} list_node_t;

//Lista doppiamente collegata
typedef struct list
{
    int numel;
    list_node_t *head;
} list_t;
