#pragma once

#ifndef _POSIX_C_SOURCE
    #define _POSIX_C_SOURCE 199309L
    #define __USE_POSIX199309
#endif
#ifndef _CRT_SECURE_NO_WARNINGS
    #define _CRT_SECURE_NO_WARNINGS
#endif

#include <time.h>

typedef struct timespec chrono_t;

#ifndef _WIN32
// Imposta l'orologio da utilizzare come riferimento
void chrono_set(clockid_t clk_id);
#endif

// Ritorna l'istante in cui viene chiamato
chrono_t chrono_now(void);

// Converte in un intero a 64 bit (non è detto che ci stia tutto!)
unsigned long long chrono_toull(chrono_t t);

// Converte in un double, in secondi, un timespec
double chrono_tod(chrono_t t);

// Ritorna la differenza fra due timespec
chrono_t chrono_sub(chrono_t end, chrono_t start);

//Ritorna la somma fra due timespec
chrono_t chrono_sum(chrono_t a, chrono_t b);

//Ritorna il tempo passato dal timespec specificato
chrono_t chrono_elap(chrono_t start);

#define CHRONO_GLUE0(x, y) x##y
#define CHRONO_GLUE(x, y) CHRONO_GLUE0(x, y)
#define CHRONO_UNIQUE(name) CHRONO_GLUE(CHRONO_GLUE(name, __), __LINE__)

#define CHRONO_STR0(x) #x
#define CHRONO_STR(x) CHRONO_STR0(x)

// Macro utile per cronometrare e stampare info
#define CHRONO_MEASURE(function, non_unique_times, times, elap, i)                                 \
    do                                                                                             \
    {                                                                                              \
        int times = non_unique_times;                                                              \
        chrono_t elap = chrono_now();                                                              \
                                                                                                   \
        for (int i = 0; i < (times); ++i)                                                          \
            function;                                                                              \
                                                                                                   \
        elap = chrono_elap(elap);                                                                  \
                                                                                                   \
        for (char *i = CHRONO_STR(function); *i != '(' && *i; ++i)                                 \
            putchar(*i);                                                                           \
                                                                                                   \
        printf(" (%d times): %lf sec. \n", times, chrono_tod(elap));                               \
    } while (0)

#define chrono_measure(function, times)                                                            \
    CHRONO_MEASURE(function, times, CHRONO_UNIQUE(times), CHRONO_UNIQUE(elap), CHRONO_UNIQUE(i))
