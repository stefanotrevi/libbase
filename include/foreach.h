#pragma once

typedef long long foreach_t;

// Macro ricorsive (max. 64 ricorsioni)
#define FOREACH_EVAL0(...) __VA_ARGS__
#define FOREACH_EVAL1(...) FOREACH_EVAL0(FOREACH_EVAL0(__VA_ARGS__))
#define FOREACH_EVAL2(...) FOREACH_EVAL1(FOREACH_EVAL1(__VA_ARGS__))
#define FOREACH_EVAL3(...) FOREACH_EVAL2(FOREACH_EVAL2(__VA_ARGS__))
#define FOREACH_EVAL4(...) FOREACH_EVAL3(FOREACH_EVAL3(__VA_ARGS__))
#define FOREACH_EVAL(...) FOREACH_EVAL4(FOREACH_EVAL4(__VA_ARGS__))

// Finisce la ricorsione
#define FOREACH_END(...)
#define FOREACH_GET_END() 0, FOREACH_END

// Testa se effettuare una nuova chiamata ricorsiva
#define FOREACH_TESTEVAL(test) (test
#define FOREACH_NEXT0(test, next, ...) next
#ifdef __ICL
    #define FOREACH_NEXT1(test, next, ...) FOREACH_NEXT0(test, next, 0)
    #define FOREACH_NEXT(test, next) FOREACH_NEXT1 FOREACH_TESTEVAL(FOREACH_GET_END test), next)
    #define FOREACH_COMMA ,
#else
    #define FOREACH_NEXT1(test, next) FOREACH_NEXT0(test, next, 0)
    #define FOREACH_NEXT(test, next) FOREACH_NEXT1(FOREACH_GET_END test, next)
    #define FOREACH_COMMA
#endif

// Dichiara le variabili
#define FOREACH_INIT0(x, v, peek, ...) \
    x, FOREACH_NEXT(peek, FOREACH_INIT1)(peek, __VA_ARGS__)
#define FOREACH_INIT1(x, v, peek, ...) \
    x, FOREACH_NEXT(peek, FOREACH_INIT0)(peek, __VA_ARGS__)

#define FOREACH_INIT(...) FOREACH_EVAL(FOREACH_INIT1(__VA_ARGS__, (), 0))

// Carica le variabili dall'array
#define FOREACH_CTRL0(i, x, v, peek, ...) \
    x = v[(foreach_t)i], FOREACH_NEXT(peek, FOREACH_CTRL1)(i, peek, __VA_ARGS__)
#define FOREACH_CTRL1(i, x, v, peek, ...) \
    x = v[(foreach_t)i], FOREACH_NEXT(peek, FOREACH_CTRL0)(i, peek, __VA_ARGS__)

#define FOREACH_CTRL(...) FOREACH_EVAL(FOREACH_CTRL1(__VA_ARGS__, (), 0))

// Salva le variabili nell'array
#define FOREACH_UPDT0(i, x, v, peek, ...) \
    v[(foreach_t)i] = x, FOREACH_NEXT(peek, FOREACH_UPDT1)(i, peek, __VA_ARGS__)
#define FOREACH_UPDT1(i, x, v, peek, ...) \
    v[(foreach_t)i] = x, FOREACH_NEXT(peek, FOREACH_UPDT0)(i, peek, __VA_ARGS__)

#define FOREACH_UPDT(...) FOREACH_EVAL(FOREACH_UPDT1(__VA_ARGS__, (), 0))

// Crea un nome "univoco" (lo scope è comunque limitato al ciclo)
#define FOREACH_PASTE0(x, y) x##y
#define FOREACH_PASTE(x, y) FOREACH_PASTE0(x, y)
#define FOREACH_UNIQUE(x) FOREACH_PASTE(FOREACH_PASTE(FOREACH_PASTE(foreach_unique_, x), _), __LINE__)


/* Funzionamento:
 * 1) INIZIALIZZAZIONE: Dichiara ed inizializza un indice di tipo "puntatore al tipo referenziato 
 * dal primo array" a 0, oltre che una variabile che "assorba" eventuali effetti collaterali di numel. 
 * Per ogni array passato dall'utente, dichiara una variabile (il nome è 
 * scelto dall'utente) di tipo "tipo referenziato dal primo array". La variabile non può essere 
 * inizializzata ora, perché la dimensione dell'array potrebbe essere <= 0.
 * 2) CONTROLLO: Controlla che l'indice (cast a foreach_t) sia minore della dimensione dell'array. 
 * Se si, procedi a inizializzare ogni variabile: var0..n = array0..n[indice] 
 * 3) AGGIORNAMENTO: Ricopia il contenuto delle variabili negli array, e aggiorna l'indice. 
*/
#define FOREACH(T, i, n, col_n, ...)                                          \
    for (T FOREACH_INIT(__VA_ARGS__) FOREACH_COMMA *i = 0, *n = (T *)(col_n); \
         i < n && (FOREACH_CTRL(i, __VA_ARGS__) FOREACH_COMMA 1);             \
         FOREACH_UPDT(i, __VA_ARGS__) FOREACH_COMMA i = (T *)((char *)i + 1))

/* foreach: itera lungo gli elementi di uno o più array. 
 * USO (se typeof è supportato):
 *  foreach(numel, var_1, array_1, ..., var_n, array_n)
 *  {
 *      do_stuff(var_1);
 *      ...
 *      do_stuff(var_n);
 *  }
 * Alcune regole:
 * I paramertri, eccetto eventualmente @numel, NON devono avere effetti collaterali
 * Tutti gli array devono avere la stessa dimensione, o comunque numel <= minlength(array1..n)
 * Tutti gli array devono avere un tipo il cui rango è <= a quello del primo array.
 * L'uso di un solo iteratore comune e di una dimensione comune garantisce le stesse identiche
 * prestazioni (e comportamento) di un ciclo del tipo:
 *  for(i = 0; i < numel; ++i)
 *  {
 *      do_stuff(array_1[i]);
 *      ...
 *      do_stuff(array_n[i]);
 *  }
 * 
*/
#ifdef _WIN32
    #define foreach(numel, type, var1, array1, ...) \
        FOREACH(type, FOREACH_UNIQUE(idx), FOREACH_UNIQUE(dim), numel, var1, array1, ##__VA_ARGS__)
#else
    #define foreach(numel, var1, array1, ...) \
        FOREACH(typeof(*array1), FOREACH_UNIQUE(idx), FOREACH_UNIQUE(dim), numel, var1, array1, ##__VA_ARGS__)
#endif
