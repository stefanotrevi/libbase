#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

// Ordina un vettore usando il metodo migliore per l'architettura (sortNoAVX/sortAVX/sortAVX512)
int *sort(int *v, int numel);

// Ordina un vettore usando un algoritmo ibrido (quickSort/countSort)
int *sortNoAVX(int *v, int numel);

// Versione di sort ottimizzata per AVX-2 (quickSortAVX/countSortAVX)
int *sortAVX(int *v, int numel);

// Versione di sort ottimizzata per AVX-512 (quickSortAVX512/countSortAVX512)
int *sortAVX512(int *v, int numel);

// Implementazione di InsertionSort
int *insertionSort(int *v, int numel);

// Implementazione vettorizzata di insertionSort
int *insertionSortAVX(int *v, int numel);

// Implementazione di BubbleSort
int *bubbleSort(int *v, int numel);

// Implementazione ottimizzata di SelectionSort
int *selectionSort(int *v, int numel);

// Implementazione ottimizzata di SelectionSort per i long
long *selectionSortl(long *v, int numel);

// Trova un elemento in un vettore ORDINATO, restituendo la sua posizione, o quella dell'elemento più vicino se non esiste
int orderedBinarySearch(int *v, int numel, int n);

// Implementazione di MergeSort
int *mergeSort(int *v, size_t numel);

// Mergesort + InsertionSort
int *hMergeSort(int *v, size_t numel);

// Implementazione di QuickSort
void ordinaRicorsivo(int *v, int dim);

// Ordina un vettore di interi conoscendo il minimo e il massimo. Se sconosciuti (entrambi = 0), li trova
int *countSort(int *v, int dim, int *sv, int sdim, int min);

// Versione vettorizzata di countSort
int *countSortAVX(int *v, int dim, int *sv, int sdim, int min);

// Effettua la ricerca binaria in un vettore, ordinandolo se necessario
size_t binarySearch(int *v, size_t numel, int n);

// Inserisce in "out" un elemento a caso del vettore "allow", e ne ritorna la posizione
int selRNG(int *out, int *allow, int adim);

// Mescola il vettore (basato su countSort)
void shuffle(int *v, int dim);

// Implementazione standard del Quicksort
void quickSort(int *v, int numel);

// Implementazione del Quicksort che non usa branching
void quickSortNonBranch(int *v, int numel);

// Implementazione del Quicksort che usa la tecnica "Collapse the walls"
void quickSortCollapse(int *v, int numel);

// Implementazione di Quicksort usando vettorizzazione AVX-2
void quickSortAVX(int *v, int numel);

// Implementazione di Quicksort usando vettorizzazione AVX-512
void quickSortAVX512(int *v, int numel);

// Ordina lessicograficamente, rispetto alla rappresentazione in bit, un array di qualsiasi tipo
void bitSort(void *v, size_t sz, size_t numel);

// Un algoritmo per disordinare in modo controllato un vettore
void DisordinaVettore(int *v, int dim, int grado);

// Funzione che compara due interi secondo gli standard di qsort
int cmpInt(const void *a, const void *b);

#ifdef __cplusplus
}
#endif
