//info nell'header
#include "supportlib.h"

int checkArray(int *v, int dim)
{
    int i, flag = 0;

    for (i = 1; i < dim; i++)
        if (v[i] < v[i - 1])
            break;
    if (i == dim)
        flag = 1;
    return flag;
}

int calcolaDim(int *v, int dim, int *average)
{
    int i, pivot, ndim1 = 0, tempAv, flag = 1;

    do
    {
        pivot = v[*average];
        for (i = 0; i < dim; i++)
            if (v[i] < pivot)
                ndim1++;
            else if (!ndim1 && v[i] > pivot && flag)
            {
                tempAv = i;
                flag = 0;
            }
        if (!ndim1)
            *average = tempAv;
    } while (!ndim1);
    return ndim1;
}

void riempiPartialArray(int *v, int dim, int ndim1, int average)
{
    int i, j = ndim1, pivot = v[average], temp;

    for (i = 0; i < ndim1 && j < dim; i++)
        if (v[i] >= pivot)
        {
            while (v[j] >= pivot && j < dim)
                j++;
            temp = v[j];
            v[j] = v[i];
            v[i] = temp;
            j++;
        }
}

int calcolaCifre(int n)
{
    int dim = 0;
    do
        dim++;
    while (n /= 10);

    return dim;
}

int tavolaEscape(char *in)
{
    if (in[0] == '\\')
        switch (in[1])
        {
        case 'n':
            printf("\n");
            return 1;
        case 't':
            printf("\t");
            return 1;
        case 'b':
            printf("\b");
            return 1;
        case 'a':
            printf("\a");
            return 1;
        case 'f':
            printf("\f");
            return 1;
        case 'r':
            printf("\r");
            return 1;
        case 'u':
            if (in[2] == '0')
                printf(SQUARE);
            else if (in[2] == '2')
                switch (in[5])
                {
                case '0':
                    printf(PEDEX0);
                    return 1;
                case '1':
                    printf(PEDEX1);
                    return 1;
                case '2':
                    printf(PEDEX2);
                    return 1;
                case '3':
                    printf(PEDEX3);
                    return 1;
                case '4':
                    printf(PEDEX4);
                    return 1;
                case '5':
                    printf(PEDEX5);
                    return 1;
                case '6':
                    printf(PEDEX6);
                    return 1;
                case '7':
                    printf(PEDEX7);
                    return 1;
                case '8':
                    printf(PEDEX8);
                    return 1;
                case '9':
                    printf(PEDEX9);
                    return 1;
                default:
                    return 0;
                }
            else
                return 0;
        case 'x':
            if (in[5] == '9')
                switch (in[6])
                {
                case '1':
                    printf(RED);
                    return 1;
                case '2':
                    printf(GREEN);
                    return 1;
                case '3':
                    printf(YELLOW);
                    return 1;
                case '4':
                    printf(BLUE);
                    return 1;
                case '5':
                    printf(MAGENTA);
                    return 1;
                case '6':
                    printf(CYAN);
                    return 1;
                default:
                    return 0;
                }
            else if (in[5] == '0')
            {
                printf("\b" RESET);
                return 1;
            }
        default:
            return 0;
        }
    else
        return 0;
}

int tavolaEscapeFile(char *in, FILE *fT)
{
    if (in[0] == '\\')
        switch (in[1])
        {
        case 'n':
            fprintf(fT, "\n");
            return 1;
        case 't':
            fprintf(fT, "\t");
            return 1;
        case 'b':
            fprintf(fT, "\b");
            return 1;
        case 'a':
            fprintf(fT, "\a");
            return 1;
        case 'f':
            fprintf(fT, "\f");
            return 1;
        case 'r':
            fprintf(fT, "\r");
            return 1;
        case 'u':
            if (in[2] == '0')
                fprintf(fT, SQUARE);
            else if (in[2] == '2')
                switch (in[5])
                {
                case '0':
                    fprintf(fT, PEDEX0);
                    return 1;
                case '1':
                    fprintf(fT, PEDEX1);
                    return 1;
                case '2':
                    fprintf(fT, PEDEX2);
                    return 1;
                case '3':
                    fprintf(fT, PEDEX3);
                    return 1;
                case '4':
                    fprintf(fT, PEDEX4);
                    return 1;
                case '5':
                    fprintf(fT, PEDEX5);
                    return 1;
                case '6':
                    fprintf(fT, PEDEX6);
                    return 1;
                case '7':
                    fprintf(fT, PEDEX7);
                    return 1;
                case '8':
                    fprintf(fT, PEDEX8);
                    return 1;
                case '9':
                    fprintf(fT, PEDEX9);
                    return 1;
                default:
                    return 0;
                }
            else
                return 0;
        case 'x':
            if (in[5] == '9')
                switch (in[6])
                {
                case '1':
                    fprintf(fT, RED);
                    return 1;
                case '2':
                    fprintf(fT, GREEN);
                    return 1;
                case '3':
                    fprintf(fT, YELLOW);
                    return 1;
                case '4':
                    fprintf(fT, BLUE);
                    return 1;
                case '5':
                    fprintf(fT, MAGENTA);
                    return 1;
                case '6':
                    fprintf(fT, CYAN);
                    return 1;
                default:
                    return 0;
                }
            else if (in[5] == '0')
            {
                fprintf(fT, "\b" RESET);
                return 1;
            }
        default:
            return 0;
        }
    else
        return 0;
}
void initArrayAdd(int *v, int dim, int vI)
{
    int i;
    v[0] = vI;
    for (i = 1; i <= dim; i++)
        v[i] = v[i - 1] + 1;
}

void reshapeMatrice(double **M1, double **M2, int righe1, int colonne1, int righe2, int colonne2)
{
    int i, j, si = 0, sj = -1;

    for (i = 0; i < righe2; i++)
    {
        if (si == righe1 - 1 && si)
            break;
        for (j = 0; j < colonne2; j++)
        {
            if (sj == colonne1 - 1)
            {
                sj = 0;
                if (si == righe1 - 1)
                    break;
                si++;
            }
            else
                sj++;
            M2[i][j] = M1[si][sj];
        }
    }
    si++;
    sj++;
}