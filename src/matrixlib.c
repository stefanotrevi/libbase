#include "matrixlib.h"

void intro(void)
{
    char in;
    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system(CLEAR);
    printf("Benvenuto nel calcolatore di matrici!\nPer mostrare l'help, premere \'h\',\nPer continuare, premere un altro tasto...\n");
    scanf("%c", &in);
    if (in == 'h')
        help();
}

void initVoidMatrix(Matrice *M)
{
    M->righe = 0;
    M->colonne = 0;
    M->nome[0] = 0;
    M->elemento = NULL;
    M->temp = 0;
}

void saveMatrix(Matrice *M, char *nome)
{
    M->temp = 0;
    strcpy(M->nome, nome);
}
void delMatrix(Matrice *M)
{
    mFree((void **)M->elemento, M->righe);
    M->righe = 0;
    M->colonne = 0;
    M->nome[0] = 0;
    M->temp = 0;
}

void fToMatrix(double f, Matrice *M, int diagonale)
{
    int i, j;
    if (diagonale)
        for (i = 0; i < M->righe; i++)
            for (j = 0; j < M->colonne; j++)
                if (i == j)
                    M->elemento[i][j] = f;
                else
                    M->elemento[i][j] = 0;
    else
        for (i = 0; i < M->righe; i++)
            for (j = 0; j < M->colonne; j++)
                M->elemento[i][j] = f;
}

void fVToMatrix(double **f, int fRighe, int fColonne, Matrice *M)
{
    int i, j;
    for (i = 0; i < M->righe && i < fRighe; i++)
        for (j = 0; j < M->colonne && j < fColonne; j++)
            M->elemento[i][j] = f[i][j];
}

void initMatrix(Matrice *M, char *nome, int righe, int colonne, double valore, int diagonale)
{
    M->colonne = colonne;
    M->righe = righe;
    M->temp = 0;
    strcpy(M->nome, nome);
    fmMalloc(&M->elemento, M->righe, M->colonne);
    fToMatrix(valore, M, diagonale);
}

void listMatrix(Matrice *M, Lista L)
{
    int i, j;
    Dato d;
    insTesta(&L, d);

    for (i = 0; i < M->righe && L && L->dato.check != '\n' && L->dato.check != '\'' && L->dato.check != ']'; i++)
    {
        L->dato.check = ' ';
        for (j = 0; j < M->colonne && L && L->dato.check != ';'; j++)
        {
            L = L->next;
            M->elemento[i][j] = L->dato.value;
        }
    }
}

void fListMatrix(Matrice *M, Lista L)
{
    int i, j;
    Dato d;
    insTesta(&L, d);

    for (i = 0; i < M->righe && L && L->dato.check != ';' && L->dato.check != '\''; i++)
    {
        L->dato.check = ' ';
        for (j = 0; j < M->colonne && L->next && L->dato.check != '\n'; j++)
        {
            L = L->next;
            M->elemento[i][j] = L->dato.value;
        }
    }
}

void getMatrix(Matrice *M)
{
    Dato buff;
    char nome[MAXL];
    Lista Ls;

    initVoidMatrix(M);
    nuovaLista(&Ls);
    buff.check = 0;
    scanf("%s", nome);
    M->colonne = 1;
    while (buff.check != '\n' && buff.check != '\'')
    {
        scanf("%lf%c", &buff.value, &buff.check);
        insCoda(&Ls, buff);
        if (!M->righe && buff.check != ';' && buff.check != '\n' && buff.check != '\'')
            M->colonne++;
        else if (buff.check == ';')
            M->righe++;
    }
    M->righe++;
    fmMalloc(&M->elemento, M->righe, M->colonne);
    listMatrix(M, Ls);
    if (buff.check == '\'')
        *M = trasposta(*M);
    M->temp = 0;
    strcpy(M->nome, nome);
}

void getStringMatrix(char *str, Matrice *M)
{
    Dato buff;
    char nome[MAXL], number[MAXL], divisor[MAXL];
    Lista Ls;
    int i = 0, j;

    initVoidMatrix(M);
    nuovaLista(&Ls);
    buff.check = 0;
    for (i = 0; str[i] != '=' && str[i] != ' ' && str[i] != 0; i++)
        nome[i] = str[i];
    nome[i] = 0;
    M->colonne = 1;
    while (buff.check != '\n' && buff.check != '\'')
    {
        while (!(isdigit(str[i]) || str[i] == '.' || str[i] == '-') && str[i] != ']')
            i++;

        j = i;
        while (isdigit(str[i]) || str[i] == '.' || str[i] == '-')
        {
            number[i - j] = str[i];
            i++;
        }
        number[i - j] = 0;
        if (str[i] == '/')
        {
            j = ++i;
            while (isdigit(str[i]) || str[i] == '.' || str[i] == '-')
            {
                divisor[i - j] = str[i];
                i++;
            }
            divisor[i - j] = 0;
            buff.value = strtod(number, NULL) / strtod(divisor, NULL);
        }
        else
            buff.value = strtod(number, NULL);
        if (str[i] != ']')
            buff.check = str[i];
        else
            buff.check = str[i + 1];
        insCoda(&Ls, buff);
        if (!M->righe && buff.check != ';' && buff.check != '\n' && buff.check != '\'' && buff.check != ']')
            M->colonne++;
        else if (buff.check == ';')
            M->righe++;
    }
    M->righe++;
    fmMalloc(&M->elemento, M->righe, M->colonne);
    listMatrix(M, Ls);
    if (buff.check == '\'')
        *M = trasposta(*M);
    M->temp = 0;
    strcpy(M->nome, nome);
}
void getFileMatrix(FILE *fT, Matrice *M)
{
    int i, j;
    char nome[MAXL];
    Dato buff;
    double divisor;
    Lista Ls;

    initVoidMatrix(M);
    nuovaLista(&Ls);
    buff.check = 0;
    M->colonne++;
    fscanf(fT, "%s", nome);
    while (buff.check != ';' && buff.check != '\'' && !feof(fT))
    {
        fscanf(fT, "%lf%c", &buff.value, &buff.check);
        if (buff.check == '/')
        {
            fscanf(fT, "%lf%c", &divisor, &buff.check);
            buff.value /= divisor;
        }
        if (Ls && buff.check == '\n' && buff.value == Ls->dato.value && buff.check == Ls->dato.check)
        {
            Ls->dato.check = ';';
            M->righe--;
            break;
        }
        else
        {
            insTesta(&Ls, buff);
            if (!M->righe && buff.check != '\n' && buff.check != '\'' && buff.check != ';')
                M->colonne++;
            else if (buff.check == '\n')
                M->righe++;
        }
    }
    M->righe++;
    Ls = reverse(Ls);
    fmMalloc(&M->elemento, M->righe, M->colonne);
    fListMatrix(M, Ls);
    eliminaLista(&Ls);
    if (buff.check == '\'')
        *M = trasposta(*M);
    M->temp = 0;
    strcpy(M->nome, nome);
}

void copiaMatrice(Matrice M1, Matrice *M2)
{
    initVoidMatrix(M2);
    M2->righe = M1.righe;
    M2->colonne = M1.colonne;
    M2->temp = M1.temp;
    strcpy(M2->nome, M1.nome);
    fmMalloc(&M2->elemento, M2->righe, M2->colonne);
    fVToMatrix(M1.elemento, M1.righe, M1.colonne, M2);
}

void stampaMatrice(Matrice M)
{
    int i, j;
    if (M.righe == 0 && M.colonne == 0)
        printf("Matrice vuota\n");
    else
    {
        printf(CYAN "%s =\n" RESET, M.nome);
        for (i = 0; i < M.righe; i++)
        {
            for (j = 0; j < M.colonne; j++)
                printf("%lf\t", M.elemento[i][j]);
            printf("\n");
        }
        printf("\n");
    }
    if (M.temp)
        delMatrix(&M);
}

double traccia(Matrice M)
{
    double traccia = 0;
    int i, j;

    if (M.colonne == M.righe)
        for (i = 0; i < M.righe; i++)
            traccia += M.elemento[i][i];

    if (M.temp)
        delMatrix(&M);
    return traccia;
}

Matrice trasposta(Matrice M)
{
    int i = 0, j;
    Matrice Mout;

    Mout.righe = M.colonne;
    Mout.colonne = M.righe;
    fmMalloc(&Mout.elemento, Mout.righe, Mout.colonne);
    Mout.temp = 1;
    while (M.nome[i])
        i++;
    M.nome[i] = '\'';
    M.nome[i + 1] = 0;
    strcpy(Mout.nome, M.nome);
    for (i = 0; i < M.righe; i++)
        for (j = 0; j < M.colonne; j++)
            Mout.elemento[j][i] = M.elemento[i][j];

    if (M.temp)
        delMatrix(&M);

    return Mout;
}

Matrice somma(Matrice M1, Matrice M2)
{
    Matrice Mout;
    int i, j;

    if (M1.colonne == M2.colonne && M1.righe == M2.righe)
    {
        strcpy(Mout.nome, "\0");
        strncat(Mout.nome, M1.nome, MAXL);
        strncat(Mout.nome, "+", MAXL - strlen(Mout.nome));
        strncat(Mout.nome, M2.nome, MAXL - strlen(Mout.nome));
        Mout.righe = M1.righe;
        Mout.colonne = M1.colonne;
        initMatrix(&Mout, Mout.nome, Mout.righe, Mout.colonne, 0, 0);
        Mout.temp = 1;
        for (i = 0; i < Mout.righe; i++)
            for (j = 0; j < Mout.colonne; j++)
                Mout.elemento[i][j] = M1.elemento[i][j] + M2.elemento[i][j];
    }
    else
    {
        initVoidMatrix(&Mout);
        Mout.temp = 1;
        printf("Le matrici hanno dimensioni diverse, non possono essere sommate!\n");
    }
    if (M1.temp)
        delMatrix(&M1);
    if (M2.temp)
        delMatrix(&M2);

    return Mout;
}

Matrice prodotto(Matrice M1, Matrice M2)
{
    Matrice Mout;

    if (M1.colonne == M2.righe)
    {
        int i, j, k;
        strcpy(Mout.nome, "\0");
        strncat(Mout.nome, M1.nome, MAXL);
        strncat(Mout.nome, "*", MAXL - strlen(Mout.nome));
        strncat(Mout.nome, M2.nome, MAXL - strlen(Mout.nome));
        Mout.righe = M1.righe;
        Mout.colonne = M2.colonne;
        initMatrix(&Mout, Mout.nome, Mout.righe, Mout.colonne, 0, 0);
        Mout.temp = 1;
        for (i = 0; i < Mout.righe; i++)
            for (j = 0; j < Mout.colonne; j++)
                for (k = 0; k < M1.colonne; k++)
                    Mout.elemento[i][j] += M1.elemento[i][k] * M2.elemento[k][j];
    }
    else
    {
        initVoidMatrix(&Mout);
        Mout.temp = 1;
        printf("Le matrici non sono moltiplicabili\n");
    }
    if (M1.temp)
        delMatrix(&M1);
    if (M2.temp)
        delMatrix(&M2);

    return Mout;
}

Matrice prodottoSc(Matrice M, double f)
{
    int i, j;
    Matrice Mout;

    copiaMatrice(M, &Mout);
    Mout.temp = 1;
    for (i = 0; i < Mout.righe; i++)
        for (j = 0; j < Mout.colonne; j++)
            Mout.elemento[i][j] *= f;

    if (M.temp)
        delMatrix(&M);

    return Mout;
}

Matrice sottoMatrice(Matrice M, int riga, int colonna)
{
    int i, j, si = 0, sj;
    Matrice Mout;

    initVoidMatrix(&Mout);
    Mout.temp = 1;
    if (M.righe > 1 && M.colonne > 1)
    {
        Mout.colonne = M.colonne - 1;
        Mout.righe = M.righe - 1;
        fmMalloc(&Mout.elemento, Mout.righe, Mout.colonne);
        for (i = 0; i < M.righe; i++)
        {
            sj = 0;
            if (i == riga)
                i++;
            for (j = 0; j < M.colonne; j++)
            {
                if (j == colonna)
                    j++;
                if (i < M.righe && j < M.colonne)
                    Mout.elemento[si][sj] = M.elemento[i][j];
                sj++;
            }
            si++;
        }
    }
    if (M.temp)
        delMatrix(&M);

    return Mout;
}

// determinante basato sullo sviluppo di Laplace
double detL(Matrice M)
{
    double d = 0;
    int i, j, s = -1;

    if (M.righe == M.colonne)
    {
        for (i = 0; i < M.righe; i++)
            if (M.elemento[0][i] && M.elemento[i][0])
                break;
        if (i == M.righe)
        {
            if (M.temp)
                delMatrix(&M);
            return 0;
        }
        if (M.righe == 1)
        {
            if (M.temp)
                delMatrix(&M);
            return M.elemento[0][0];
        }
        else
        {
            for (j = 0; j < M.colonne; j++)
            {
                cambiaSegno(&s);
                d = d + s * M.elemento[0][j] * detL(sottoMatrice(M, 0, j));
            }
            if (M.temp)
                delMatrix(&M);
            return d;
        }
    }
    else
        printf("Matrice non quadrata! Impossibile calcolare il determinante.\n");
    if (M.temp)
        delMatrix(&M);
}

double det(Matrice M)
{
    double d;
    if (M.righe == M.colonne)
        d = gauss(M, 1);
    else
    {
        printf("Matrice non quadrata! Impossibile calcolare il determinante.\n");
        d = 0;
    }
    if (M.temp)
        delMatrix(&M);
    return d;
}

Matrice adj(Matrice M)
{
    Matrice Mout;

    initVoidMatrix(&Mout);
    Mout.temp = 1;

    if (M.righe == M.colonne != 0)
    {
        int s = -1, i, j;

        Mout.righe = M.righe;
        Mout.colonne = M.colonne;
        fmMalloc(&Mout.elemento, Mout.righe, Mout.colonne);
        if (M.righe > 1)
            for (i = 0; i < Mout.righe; i++)
                for (j = 0; j < Mout.colonne; j++)
                    Mout.elemento[i][j] = pow(s, i + j) * det(sottoMatrice(M, i, j));
        else
            Mout.elemento[0][0] = M.elemento[0][0];

        strcpy(Mout.nome, "\0");
        strncat(Mout.nome, "adj(", MAXL);
        strncat(Mout.nome, M.nome, MAXL - strlen(Mout.nome));
        strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));
    }
    else
        printf("Matrice vuota, impossibile calcolare l'aggiunta!\n");
    if (M.temp)
        delMatrix(&M);
    return Mout;
}

Matrice inversa(Matrice M)
{
    double d = det(M);
    Matrice Mout;

    if (!d)
    {
        initVoidMatrix(&Mout);
        Mout.temp = 1;
        if (M.temp)
            delMatrix(&M);
        return Mout;
    }

    Mout = prodottoSc(trasposta(adj(M)), 1 / d);
    strcpy(Mout.nome, "\0");
    strncat(Mout.nome, "inv(", MAXL);
    strncat(Mout.nome, M.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));
    if (M.temp)
        delMatrix(&M);
    return Mout;
}

Matrice cambiaBaseC(Matrice A, Matrice B, Matrice B1)
{
    /*
    1) trovo le coordinate delle immagini di B in C (ans = A*B),
    2) le esprimo rispetto a B1 (ans = inv(B1)*ans).
    */
    Matrice Mout = prodotto(inversa(B1), prodotto(A, B));
    strcpy(Mout.nome, "\0");
    strncat(Mout.nome, A.nome, MAXL);
    strncat(Mout.nome, "(", MAXL - strlen(Mout.nome));
    strncat(Mout.nome, B.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ", ", MAXL - strlen(Mout.nome));
    strncat(Mout.nome, B1.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));
    if (A.temp)
        delMatrix(&A);
    if (B.temp)
        delMatrix(&B);
    if (B1.temp)
        delMatrix(&B1);
    return Mout;
}

Matrice cambiaBase(Matrice A, Matrice B, Matrice B1, Matrice D, Matrice D1)
{
    /*
    1) Esprimo B nella base D (ans = inv(B)*D)
    2) Trovo le coordinate delle immagini di D in B1 (ans = A*ans)
    3) Per passare da B1 a D1: (ans = inv(D1)*B1*ans)
    */
    Matrice Mout = prodotto(prodotto(inversa(D1), B1), prodotto(A, prodotto(inversa(B), D)));

    strcpy(Mout.nome, "\0");
    strncat(Mout.nome, A.nome, MAXL);
    strncat(Mout.nome, "(", MAXL - strlen(Mout.nome));
    strncat(Mout.nome, D.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ", ", MAXL - strlen(Mout.nome));
    strncat(Mout.nome, D1.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));
    if (A.temp)
        delMatrix(&A);
    if (B.temp)
        delMatrix(&B);
    if (B1.temp)
        delMatrix(&B1);
    if (D.temp)
        delMatrix(&D);
    if (D1.temp)
        delMatrix(&D1);
    return Mout;
}

int riduciRC(Matrice *M, int rc, int select)
{
    int i, j;

    if (select == 1)
    {
        M->righe--;
        for (i = rc; i < M->righe; i++)
            for (j = 0; j < M->colonne; j++)
                M->elemento[i][j] = M->elemento[i + 1][j];
        free(M->elemento[i + 1]);
    }
    else if (select == 0)
    {
        M->colonne--;
        for (i = 0; i < M->righe; i++)
            for (j = rc; j < M->colonne; j++)
                M->elemento[i][j] = M->elemento[i][j + 1];
        free(M->elemento[j + 1]);
    }
    else
    {
        printf("Opzione selezionata invalida per la riduzione di righe/colonne.\n");
        return 0;
    }
    if (M->righe == M->colonne)
        return 1;
    else
        return 0;
}

int aggiornaV(int *v, int dim, int limit)
{
    int i, j = 1;

    //se si tratta di un nuovo array
    if (v[0] == -1)
    {
        for (i = 0; i < dim; i++)
            v[i]++;
        return 1;
    }

    //se si tratta di un array già "avviato"
    else
        for (i = 0; i < dim; i++)
            if (v[dim - j] < limit - j)
            {
                v[dim - j]++;
                for (i = dim - j + 1; i < dim; i++)
                    v[i] = v[i - 1] + 1;
                return 1;
            }
            else
                j++;
    return 0;
}

Matrice incrocio(Matrice M, int *righe, int *colonne)
{
    Matrice Mout;
    int i = 0, j = 0;

    initVoidMatrix(&Mout);
    while (righe[Mout.righe] >= 0)
        Mout.righe++;
    while (colonne[Mout.colonne] >= 0)
        Mout.colonne++;
    if (Mout.righe == M.righe && Mout.colonne == M.colonne)
        return M;
    fmMalloc(&Mout.elemento, Mout.righe, Mout.colonne);
    Mout.temp = 1;
    for (i = 0; i < Mout.righe; i++)
        for (j = 0; j < Mout.colonne; j++)
            Mout.elemento[i][j] = M.elemento[righe[i]][colonne[j]];
    if (M.temp)
        delMatrix(&M);
    return Mout;
}

int rango(Matrice M)
{
    //Se è vuota
    if (!M.righe || !M.colonne)
    {
        printf("Matrice vuota, impossibile calcolare il rango!\n");
        if (M.temp)
            delMatrix(&M);
        return 0;
    }
    //Se è quadrata, basta applicare gauss
    else if (M.righe == M.colonne)
        return rGauss(M, 1);
    //Altrimenti devo prendere tutte le s.m. quadrate di dim. massima, e applicare gauss
    else
    {
        int *vS, *vD, rank, min;
        //Devo essere sicuro che la matrice non degeneri
        while (M.righe > 1 || M.colonne > 1)
        {
            //Inizializzo il minimo fra colonne e righe, e i rispettivi vettori d'incrocio, con terminatore -1
            min = intMin(M.righe, M.colonne);
            vS = (int *)malloc((min + 1) * sizeof(int));
            vD = (int *)malloc((min + 1) * sizeof(int));
            initArrayAdd(vS, min, 0);
            initArrayAdd(vD, min, -1);
            vS[min] = -1;
            vD[min] = -1;
            //Se il le righe sono statiche
            if (M.righe < M.colonne)
            {
                //Eseguo tutti gli incroci possibili
                while (aggiornaV(vD, min, min))
                {
                    rank = rGauss(incrocio(M, vS, vD), 0);
                    if (rank == min)
                    {
                        free(vS);
                        free(vD);
                        if (M.temp)
                            delMatrix(&M);
                        return rank;
                    }
                }
                //A questo punto, significa che il rango < max, quindi escludo una colonna
                //Se la matrice risultante è quadrata, ci riconduciamo al caso rispettivo
                if (riduciRC(&M, M.colonne - 1, 0))
                {
                    free(vS);
                    free(vD);
                    return rGauss(M, 1);
                }
            }
            //Se le colonne sono statiche (il codice è praticamente identico)
            else
            {
                while (aggiornaV(vD, min, min))
                {
                    rank = rGauss(incrocio(M, vD, vS), 0);
                    if (rank == min)
                    {
                        free(vS);
                        free(vD);
                        if (M.temp)
                            delMatrix(&M);
                        return rank;
                    }
                }
                if (riduciRC(&M, M.colonne - 1, 1))
                {
                    free(vS);
                    free(vD);
                    return rGauss(M, 1);
                }
                return 0;
            }
        }
    }
}

double gauss(Matrice M, int block)
{
    double *sV, x;
    int i = 1, j, s = 0;
    Matrice Ms;

    copiaMatrice(M, &Ms);

    if (Ms.righe == 1)
    {
        x = Ms.elemento[0][0];
        if (M.temp)
            delMatrix(&M);
        delMatrix(&Ms);
        return x;
    }
    else
    {
        //Se la prima riga ha elemento nullo...
        if (!Ms.elemento[0][0])
        {
            //cerco una riga con elemento non nullo
            while (i < Ms.righe && !Ms.elemento[i][0])
                i++;
            //Se la trovo...
            if (i != Ms.righe)
            //Scambio la prima riga con la riga trovata
            {
                sV = (double *)malloc(Ms.colonne * sizeof(double));
                for (j = 0; j < Ms.colonne; j++)
                {
                    sV[j] = Ms.elemento[i][j];
                    Ms.elemento[i][j] = Ms.elemento[0][j];
                    Ms.elemento[0][j] = sV[j];
                }
                free(sV);
            } //Altrimenti il determinante è 0
            else
            {
                if (M.temp)
                    delMatrix(&M);
                delMatrix(&Ms);
                return 0;
            }
            //Il segno cambia
            s = 1;
        }
        //Per ogni riga > 1 con primo elemento non nullo
        for (i; i < Ms.righe; i++)
            if (Ms.elemento[i][0])
            {
                /*Sostituisco la riga con essa stessa sommata alla prima riga moltiplicata per "x",
                dove x è quel numero che, moltiplicato per il primo elemento della prima riga,
                fa si che M[i][0] + M[0][0] = 0, perciò x = -M[i][0]/M[0][0]*/
                x = -(Ms.elemento[i][0] / Ms.elemento[0][0]);
                for (j = 0; j < Ms.colonne; j++)
                    Ms.elemento[i][j] += Ms.elemento[0][j] * x;
            }
        x = Ms.elemento[0][0] * gauss(sottoMatrice(Ms, 0, 0), 0);
        if (M.temp)
            delMatrix(&M);
        delMatrix(&Ms);
        return s ? -x : x;
    }
}

int rGauss(Matrice M, int block)
{
    double *sV, x;
    int i = 1, j;
    Matrice Ms;

    if (block)
        copiaMatrice(M, &Ms);
    else
        Ms = M;
    if (Ms.righe == 1 && Ms.elemento[0][0])
    {
        if (M.temp)
            delMatrix(&M);
        delMatrix(&Ms);
        return 1;
    }
    else if (Ms.righe == 1 && !Ms.elemento[0][0])
    {
        if (M.temp)
            delMatrix(&M);
        delMatrix(&Ms);
        return 0;
    }
    else
    {
        //Se la prima riga ha elemento nullo...
        if (!Ms.elemento[0][0])
        {
            //cerco una riga con elemento non nullo
            while (i < Ms.righe && !Ms.elemento[i][0])
                i++;
            //Se la trovo...
            if (i != Ms.righe)
            //Scambio la prima riga con la riga trovata
            {
                sV = (double *)malloc(Ms.colonne * sizeof(double));
                for (j = 0; j < Ms.colonne; j++)
                {
                    sV[j] = Ms.elemento[i][j];
                    Ms.elemento[i][j] = Ms.elemento[0][j];
                    Ms.elemento[0][j] = sV[j];
                }
                free(sV);
            } //Altrimenti il determinante è 0
            else
            {
                x = rGauss(sottoMatrice(Ms, 0, 0), 0);
                if (M.temp)
                    delMatrix(&M);
                delMatrix(&Ms);
                return x;
            }
        }
        //Per ogni riga > 1 con primo elemento non nullo
        for (i; i < Ms.righe; i++)
            if (Ms.elemento[i][0])
            {
                /*Sostituisco la riga con essa stessa sommata alla prima riga moltiplicata per "x",
                dove x è quel numero che, moltiplicato per il primo elemento della prima riga,
                fa si che M[i][0] + M[0][0] = 0, perciò x = -M[i][0]/M[0][0]*/
                x = -(Ms.elemento[i][0] / Ms.elemento[0][0]);
                for (j = 0; j < Ms.colonne; j++)
                    Ms.elemento[i][j] += Ms.elemento[0][j] * x;
            }
        if (Ms.elemento[0][0])
        {
            x = 1 + rGauss(sottoMatrice(Ms, 0, 0), 0);
            if (M.temp)
                delMatrix(&M);
            delMatrix(&Ms);
            return x;
        }
        else
        {
            x = rGauss(sottoMatrice(Ms, 0, 0), 0);
            if (M.temp)
                delMatrix(&M);
            delMatrix(&Ms);
            return x;
        }
    }
}

int simmetrica(Matrice M)
{
    int i, j, flag = 1;

    if (M.righe != M.colonne)
        flag = 0;
    else
        for (i = 0; i < M.righe; i++)
            for (j = 0; j < M.colonne; j++)
                if (M.elemento[i][j] != M.elemento[j][i])
                {
                    if (M.temp)
                        delMatrix(&M);
                    return 0;
                }
    if (M.temp)
        delMatrix(&M);
    return flag;
}

double *fQ(Matrice M, int *dim)
{
    char is_temp = M.temp;

    M.temp = 0;
    if (simmetrica(M))
    {
        int i, j = 0;
        double *vC;
        *dim = 0;
        for (i = M.righe; i; i--)
            *dim += i;
        vC = (double *)malloc(*dim * sizeof(double));
        while (M.righe > 1)
        {
            for (i = 0; i < M.colonne; i++)
                if (i)
                    vC[i + j] = M.elemento[0][i] + M.elemento[i][0];
                else
                    vC[i + j] = M.elemento[0][0];
            j += i;
            M = sottoMatrice(M, 0, 0);
        }
        vC[j] = M.elemento[0][0];
        M.temp = is_temp;
        if (M.temp)
            delMatrix(&M);
        return vC;
    }
    else
    {
        M.temp = is_temp;
        if (M.temp)
            delMatrix(&M);
        return 0;
    }
}

void stampaFQ(Matrice M, double *vC)
{
    int is_temp = M.temp;

    M.temp = 0;
    if (simmetrica(M))
    {
        int i, j, move = M.righe, dim = 0, cifre = M.righe / 10 + 1;
        char **pedex1, **pedex2, mainP[] = "\\u2080";

        //Devo allocare la memoria per i pedici, che dipende da quante cifre hanno gli indici
        mStrMalloc(&pedex1, mainP, cifre);
        mStrMalloc(&pedex2, mainP, cifre);

        for (i = M.righe; i; i--)
            dim += i;
        printf("Forma quadratica di " CYAN "%s" RESET ":\n", M.nome);
        //Stampa del primo elemento della prima riga
        if (vC[0])
        {
            printf("%lf" BLUE "x", vC[0]);
            stampaSpeciale(pedex1[0]);
            printf(RED SQUARE);
            if (move - 1)
            {
                //stampa degli elementi successivi
                for (j = 1; j < move; j++)
                {
                    aggiornaMStr(pedex2, 5, cifre, '9');
                    if (vC[j])
                    {
                        printf(RESET " + %lf" BLUE "x", vC[j]);
                        stampaSpeciale(pedex1[0]);
                        printf("x");
                        stampaCifreSp(pedex2, 5, cifre);
                    }
                }
            }
            else
                printf(RED SQUARE RESET);
        }
        //Stampa delle altre righe
        for (i = move; i < dim; i += move)
        {
            move--;
            aggiornaMStr(pedex1, 5, cifre, '9');
            if (vC[i])
            {
                printf(RESET " + %lf" BLUE "x", vC[i]);
                stampaCifreSp(pedex1, 5, cifre);
                if (move - 1)
                {
                    printf(RED SQUARE RESET);
                    for (j = 0; j < cifre; j++)
                        pedex2[j][5] = pedex1[j][5];
                    for (j = 1; j < move; j++)
                    {
                        aggiornaMStr(pedex2, 5, cifre, '9');
                        if (vC[i + j])
                        {
                            printf(RESET " + %lf" BLUE "x", vC[i + j]);
                            stampaSpeciale(pedex1[0]);
                            printf("x");
                            stampaCifreSp(pedex2, 5, cifre);
                        }
                    }
                }
                else
                    printf(RED SQUARE RESET);
            }
        }
        printf("\n");
        printf(RESET);
        mFree((void **)pedex1, cifre);
        mFree((void **)pedex2, cifre);
    }
    else
        printf("Matrice non simmetrica!\n");
    M.temp = is_temp;
    if (M.temp)
        delMatrix(&M);
}

Matrice reshape(Matrice M, int righe, int colonne)
{
    int dim_M = M.righe * M.colonne, dim_Mout = colonne * righe;
    Matrice Mout;

    strcpy(Mout.nome, "\0");
    strncat(Mout.nome, "reshape(", MAXL);
    strncat(Mout.nome, M.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));
    initMatrix(&Mout, Mout.nome, righe, colonne, 0, 0);
    if (dim_M <= dim_Mout)
    {
        if (dim_M < dim_Mout)
            printf("La matrice di output %s è più grande della matrice di input %s, tutti gli elementi dopo il %d° saranno = 0", Mout.nome, M.nome, dim_M);
        reshapeMatrice(M.elemento, Mout.elemento, M.righe, M.colonne, Mout.righe, Mout.colonne);
    }
    else if (dim_M > dim_Mout)
    {
        printf("La matrice %s è più piccola della matrice %s, %d elementi non saranno copiati\n", Mout.nome, M.nome, dim_M - dim_Mout);
        reshapeMatrice(M.elemento, Mout.elemento, M.righe, M.colonne, Mout.righe, Mout.colonne);
    }
    if (M.temp)
        delMatrix(&M);
    return Mout;
}

void scambiaRighe(Matrice *M, int r1, int r2)
{
    int i;
    double *temp;

    if (r1 >= M->righe || r2 >= M->righe)
        printf("Le righe da scambiare escono dalla matrice!\n");
    else
    {
        temp = M->elemento[r1];
        M->elemento[r1] = M->elemento[r2];
        M->elemento[r2] = temp;
    }
}

void scambiaColonne(Matrice *M, int c1, int c2)
{
    int i, temp;

    if (c1 >= M->colonne || c2 >= M->colonne)
        printf("Le righe da scambiare escono dalla matrice!\n");
    else
        for (i = 0; i < M->righe; i++)
        {
            temp = M->elemento[i][c1];
            M->elemento[i][c1] = M->elemento[i][c2];
            M->elemento[i][c2] = temp;
        }
}

Matrice sistema(Matrice A, Matrice b)
{
    //Matrice Mout = prodotto(inversa(A), b);
    Matrice Mout;

    strcpy(Mout.nome, "\0");
    strncat(Mout.nome, "x(", MAXL);
    strncat(Mout.nome, A.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ", ", MAXL - strlen(Mout.nome));
    strncat(Mout.nome, b.nome, MAXL - strlen(Mout.nome));
    strncat(Mout.nome, ")", MAXL - strlen(Mout.nome));

    if (A.temp)
        delMatrix(&A);
    if (b.temp)
        delMatrix(&b);
    return Mout;
}