#include "baselib.h"


#include INTRIN_H
#include <inttypes.h>
#include <stdlib.h>
#include <limits.h>

void init(void)
{
#ifdef _WIN32
    system("chcp 65001");
#endif
}

void waitForKey(void)
{
    char c;

    scanf("%c", &c);
}

void quit(void)
{
    printf("Premere un tasto per uscire:\n");
    waitForKey();
}

FILE *openFile(char *nome, char *modo)
{
    FILE *f;

    if (nome)
        f = fopen(nome, modo);
    else
    {
        char fName[FILENAME_MAX];

        printf("Inserire nome file:\n");
        scanf("%s", fName);
        f = fopen(fName, modo);
    }

    if (!f)
        perror("Errore");

    return f;
}

int *RNG(int *out, int min, int max, size_t numel)
{
    int range = max - min + 1;

    for (size_t i = 0; i < numel; ++i)
        out[i] = rand() % (range) + min;

    return out;
}

int *arrayPrint(const int *v, size_t numel)
{
    putchar('{');
    if (numel)
    {
        printf("%d", v[0]);
        for (size_t i = 1; i < numel; ++i)
            printf(", %d", v[i]);
    }

    printf("}\n");
    return (int *)v;
}

int *arraySlideToR(int *v, size_t numel, size_t pos)
{
    for (size_t i = 1, j = numel - i; i < numel - pos; j = numel - ++i)
        v[j] = v[j - 1];

    return v;
}

int arrayCheckOrder(int *v, int numel)
{
    for (int i = 1; i < numel; ++i)
        if (v[i] < v[i - 1])
            return 0;

    return 1;
}

int2_t arrayMinMax(int *v, int numel)
{
    int2_t mm = {v[0], v[0]};

    for (int i = 1; i < numel; ++i)
    {
        if (v[i] < mm.x)
            mm.x = v[i];
        if (mm.y < v[i])
            mm.y = v[i];
    }

    return mm;
}
/*
int2_t arrayMinMaxAVX(int *v, int numel)
{
    int2_t mm = {v[0], v[0]};
    int sz = numel - (numel % 8);

    for (int i = 0; i < sz; i += 8)
    {
        int t1, t2;
        __m256i cmp_v;
        __m256i min_v = _mm256_loadu_si256((const __m256i *)(v + i));
        __m256i max_v = min_v;

        cmp_v = _mm256_shuffle_epi32(min_v, 0b10110001);
        min_v = _mm256_min_epi32(min_v, cmp_v);
        cmp_v = _mm256_shuffle_epi32(min_v, 0b01001110);
        min_v = _mm256_min_epi32(min_v, cmp_v);
        t1 = _mm256_cvtsi256_si32(min_v);
        t2 = _mm_cvtsi128_si32(_mm256_extractf128_si256(min_v, 1));
        t1 = t1 < t2 ? t1 : t2;
        mm.x = mm.x < t1 ? mm.x : t1;

        cmp_v = _mm256_shuffle_epi32(max_v, 0b10110001);
        max_v = _mm256_max_epi32(max_v, cmp_v);
        cmp_v = _mm256_shuffle_epi32(max_v, 0b01001110);
        max_v = _mm256_max_epi32(max_v, cmp_v);
        t1 = _mm256_cvtsi256_si32(max_v);
        t2 = _mm_cvtsi128_si32(_mm256_extractf128_si256(max_v, 1));
        t1 = t1 > t2 ? t1 : t2;
        mm.y = mm.y > t1 ? mm.y : t1;
    }

#pragma unroll(8)
#pragma novector
    for (int i = sz; i < numel; ++i)
    {
        if (v[i] < mm.x)
            mm.x = v[i];
        if (v[i] > mm.y)
            mm.y = v[i];
    }

    return mm;
}
*/
int ipow(int base, int exp)
{
    if (base <= 0)
        return 0;

    int result = 1;

    while (exp)
    {
        if (exp & 1)
            result *= base;
        base *= base;
        exp >>= 1;
    }

    return result;
}

int ilog2(int n)
{
    int rev = _lzcnt_u32(n);

    return (CHAR_BIT * sizeof(n)) - rev;
}

int round2(int n)
{
    return 1 << ilog2(n);
}

void memcpyAVX(void *dst_void, const void *src_void, size_t sz)
{
#define assign(type)                                                                               \
    {                                                                                              \
        *(type *)dst = *(const type *)src;                                                         \
        dst += sizeof(type);                                                                       \
        src += sizeof(type);                                                                       \
    }

    uint8_t *dst = (uint8_t *)dst_void;
    const uint8_t *src = (const uint8_t *)src_void;

    while (sz >= sizeof(__m256i))
    {
        __m256i block = _mm256_loadu_si256((const __m256i *)src);
        _mm256_storeu_si256((__m256i *)dst, block);

        dst += sizeof(__m256i);
        src += sizeof(__m256i);
        sz -= sizeof(__m256i);
    }

    if (!sz)
        return;
    if (sz-- & 1)
    {
        assign(int8_t);
    }

    switch (sz)
    {
    case 24: assign(uint64_t);
    case 16: assign(uint64_t);
    case 8: assign(uint64_t); return;

    case 30: assign(uint64_t);
    case 22: assign(uint64_t);
    case 14: assign(uint64_t);
    case 6: assign(uint32_t);
    case 2: assign(uint16_t); return;

    case 28: assign(uint64_t);
    case 20: assign(uint64_t);
    case 12: assign(uint64_t);
    case 4: assign(uint32_t); return;

    case 26: assign(uint64_t);
    case 18: assign(uint64_t);
    case 10:
        assign(uint64_t);
        assign(uint32_t);
        return;

    default: return;
    }

#undef assign
}
