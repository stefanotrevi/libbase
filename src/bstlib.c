#include "bstlib.h"

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdio.h>
#include <stdlib.h>

void bstInit(bst_t *t)
{
    bst_node_t *temp = calloc(1, sizeof(*temp));

    t->root = temp;
    t->numel = 0;
}

void bstDelete(bst_t *T)
{
    bstDeleteNodes(T->root);
    T->root = NULL;
    T->numel = 0;
}

void bstDeleteNodes(bst_node_t *n)
{
    if (n->left)
        bstDeleteNodes(n->left);
    if (n->right)
        bstDeleteNodes(n->right);
    free(n);
}

bst_t *bstPut(bst_t *T, int key)
{
    //Se l'albero è vuoto, inserisco nella radice
    if (!T->numel)
        T->root->key = key;
    else
    {
        bst_node_t *temp = malloc(sizeof(*temp));

        //Il nodo sarà certamente una foglia
        temp->key = key;
        temp->father = T->root;
        temp->left = temp->right = NULL;
        //Ciclo finché non ho posizionato il nodo
        while (temp->father->left != temp && temp->father->right != temp)
            //Se il padre è maggiore
            if (temp->father->key >= temp->key)
                //Se esiste la sinistra, confronto con la sinistra
                if (temp->father->left)
                    temp->father = temp->father->left;
                //Altrimenti il nodo diventa la sinistra
                else
                    temp->father->left = temp;
            //Se il padre è minore, ed esiste la destra, confronto con la destra
            else if (temp->father->right)
                temp->father = temp->father->right;
            //Altrimenti il nodo diventa la destra
            else
                temp->father->right = temp;
    }
    ++T->numel;

    return T;
}

int bstRemove(bst_t *T, bst_node_t *n)
{
    //Se l'albero è vuoto, errore
    if (!T->numel)
        return BST_ERROR;
    //Se è una foglia, o se è una pseudo-lista verso destra
    if (!n->left)
        bstPlant(T, n, n->right);
    //Se è una pseudo-lista sinistra
    else if (!n->right)
        bstPlant(T, n, n->left);
    //Se è un nodo con due figli
    else
    {
        bst_node_t *temp;

        //Cerco di bilanciare, se sono vicino al minimo/massimo prendo il successore/predecessore.
        if (bstMax(*T)->key - n->key >= n->key - bstMin(*T)->key)
        {
            temp = bstNext(n);
            if (temp->father != n)
            {
                bstPlant(T, temp, temp->right);
                temp->right = n->right;
                temp->right->father = temp;
            }
            bstPlant(T, n, temp);
            temp->left = n->left;
            temp->left->father = temp;
        }
        else
        {
            temp = bstPrec(n);
            if (temp->father != n)
            {
                bstPlant(T, temp, temp->left);
                temp->left = n->left;
                temp->left->father = temp;
            }
            bstPlant(T, n, temp);
            temp->right = n->right;
            temp->right->father = temp;
        }
    }
    //Elimino effettivamente il nodo
    free(n);
    --T->numel;

    return BST_SUCCESS;
}

bst_t *bstPlant(bst_t *T, bst_node_t *n, bst_node_t *m)
{
    bst_node_t *fat = n->father;

    if (!fat)
        T->root = m;
    else if (fat->left == n)
        fat->left = m;
    else
        fat->right = m;
    if (m)
        m->father = fat;

    return T;
}

bst_t bstPrintOrd(bst_t T)
{
    if (T.numel)
        bstPrintOrec(T);
    else
        printf("[]");

    putchar('\n');

    return T;
}

void bstPrintOrec(bst_t T)
{
    bst_node_t *temp = T.root;

    if ((T.root = temp->left))
    {
        bstPrintOrec(T);
        printf(", ");
    }
    printf("%d", temp->key);
    if ((T.root = temp->right))
    {
        printf(", ");
        bstPrintOrec(T);
    }
}

bst_t bstPrintPre(bst_t T)
{
    if (T.numel)
        bstPrintPrerec(T);
    else
        printf("[]");
    putchar('\n');

    return T;
}

void bstPrintPrerec(bst_t T)
{
    bst_node_t *temp = T.root;

    printf("%d", temp->key);
    if ((T.root = temp->left))
    {
        printf(", ");
        bstPrintPrerec(T);
    }
    if ((T.root = temp->right))
    {
        printf(", ");
        bstPrintPrerec(T);
    }
}

bst_node_t *bstFind(bst_t T, int n)
{
    //Se è vuoto
    if (!T.numel)
        return NULL;
    //Se coincide, o la radice è NULL, ritorno la radice
    if (!T.root || T.root->key == n)
        return T.root;
    //Altrimenti ripeto, o a destra o a sinistra
    if (T.root->key > n)
    {
        T.root = T.root->left;
        return bstFind(T, n);
    }
    T.root = T.root->right;

    return bstFind(T, n);
}

bst_node_t *bstMin(bst_t T)
{
    //Se non è vuoto, scorro "tutto a sinistra"
    if (!T.numel)
        return NULL;

    while (T.root->left)
        T.root = T.root->left;

    return T.root;
}

bst_node_t *bstMax(bst_t T)
{
    //Se non è vuoto, scorro "tutto a destra"
    if (!T.numel)
        return NULL;

    while (T.root->right)
        T.root = T.root->right;

    return T.root;
}

bst_node_t *bstNext(bst_node_t *n)
{
    if (n->right)
    {
        bst_t temp;

        //Temp è solo un placeholder, numel significa "non essere vuoto"
        temp.numel = 1;
        temp.root = n->right;
        return bstMin(temp);
    }
    //Salgo finché il nodo è figlio destro, o finché si può salire
    while (n->father && n->father->right == n)
        n = n->father;
    //Se sono alla radice, non esiste un successore (n == BSTmax), e il ritorno sarà NULL
    return n->father;
}

bst_node_t *bstPrec(bst_node_t *n)
{
    if (n->left)
    {
        bst_t temp;

        //Temp è solo un placeholder, numel significa "non essere vuoto"
        temp.numel = 1;
        temp.root = n->left;
        return bstMax(temp);
    }
    //Salgo finché il nodo è figlio destro, o finché si può salire
    while (n->father && n->father->left == n)
        n = n->father;
    //Se sono alla radice, non esiste un successore (n == BSTmax), e il ritorno sarà NULL
    return n->father;
}
