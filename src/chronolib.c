#include "chronolib.h"

#define MIL (1000L * 1000L)
#define BIL (MIL * 1000L)

#ifndef _WIN32
static clockid_t wall_clock = CLOCK_BOOTTIME;

void chrono_set(clockid_t clk)
{
    wall_clock = clk;
}
#endif

chrono_t chrono_now(void)
{
    chrono_t now;

#ifdef _WIN32
    timespec_get(&now, TIME_UTC);
#else
    clock_gettime(wall_clock, &now);
#endif

    return now;
}

unsigned long long chrono_toull(chrono_t t)
{
    // se tv_sec fosse a 32bit, avremmo overflow dopo 2 secondi!
    return (unsigned long long)t.tv_sec * BIL + t.tv_nsec;
}

double chrono_tod(chrono_t t)
{
    /* In molti sistemi tv_nsec ha risoluzione di 1us, quindi per evitare overflow 
    e per ridurre l'errore di incolonnamento, convertiamo in us*/
    return (double)(t.tv_sec * MIL + t.tv_nsec / 1000L) / MIL;
}

chrono_t chrono_sub(chrono_t end, chrono_t start)
{
    chrono_t diff;

    diff.tv_sec = end.tv_sec - start.tv_sec;
    //riporto
    if ((diff.tv_nsec = end.tv_nsec - start.tv_nsec) < 0L)
    {
        --diff.tv_sec;
        diff.tv_nsec += BIL;
    }

    return diff;
}

chrono_t chrono_sum(chrono_t a, chrono_t b)
{
    chrono_t sum;

    sum.tv_nsec = a.tv_nsec + b.tv_nsec;
    //riporto
    if ((sum.tv_sec = a.tv_sec + b.tv_sec) > BIL)
    {
        ++sum.tv_sec;
        sum.tv_nsec -= BIL;
    }

    return sum;
}

chrono_t chrono_elap(chrono_t start)
{
    return chrono_sub(chrono_now(), start);
}
