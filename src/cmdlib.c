#include "cmdlib.h"

int v_dim = 0;
Matrice vars[VARL];

void cmdShell(void)
{
    command cmd;

    intro();
    system(CLEAR);
    do
    {
        printf(">> ");
        cmd = readCmd();
        selectCmd(cmd);
        flusher();
    } while (strcmp("exit", cmd.var));
}

void spostaStringa(char **str, int q)
{
    int i = q;

    while (str[0][i] == ' ')
        i++;
    i++;
    while (str[0][i] == ' ')
        i++;
    str[0] = &str[0][i];
}

command readCmd(void)
{
    command cmd;
    char *input, *temp, dim = MAXL, op;
    int i, j = 0;

    input = malloc(dim * sizeof(*input));
    temp = input;
    //Inizializzo i campi come stringhe vuote
    cmd.var[0] = 0;
    cmd.funct[0] = 0;
    cmd.print = 1;
    for (i = 0; i < MAXL; i++)
        cmd.arg[i][0] = 0;

    //Scansioni l'input, spazi inclusi
    scanf("%[^\n]s", input);

    //Cerco gli operatori
    op = cercaOperatore(input);

    //Cerco e salvo i nomi delle variabili da assegnare
    if (op == '=' || !op)
        op = elaboraInput(cmd.var, &input, op);

    //Cerco e salvo il nome della funzione (al momento solo una)
    if (op == '(')
    {
        op = elaboraInput(cmd.funct, &input, op);
        while (op == ',' || op == ')')
        {
            op = elaboraInput(cmd.arg[j], &input, op);
            j++;
        }
        j = 0;
    }
    //Se si sta definendo una matrice
    else if (op == '[')
    {
        for (i = 0, j = 0; input[i] != ']' && input[i]; i++, j++)
        {
            cmd.funct[j] = input[i];
            if (input[i] == ';')
            {
                j--;
                while (cmd.funct[j] == ' ')
                {
                    cmd.funct[j] = ';';
                    j--;
                }
                j++;
            }
        }
        if (input[i])
            cmd.funct[i] = input[i];
        if (input[i + 1] == '\'')
        {
            cmd.funct[i + 1] = '\'';
            cmd.funct[i + 2] = 0;
        }
        else
            cmd.funct[i + 1] = 0;

        spostaStringa(&input, i);
        if (cercaChar(input, ';'))
            cmd.print = 0;
        return cmd;
    }
    //Se la funzione è un operatore
    else if (op)
    {
        cmd.funct[0] = op;
        cmd.funct[1] = 0;
        op = elaboraInput(cmd.arg[0], &input, op);
        elaboraInput(cmd.arg[1], &input, op); //Non assegno op per non perdere l'eventuale ;
    }

    //Cerco il carattere di blocco della stampa
    if (op == ';')
        cmd.print = 0;
    free(temp);

    return cmd;
}

void selectCmd(command cmd)
{
    int v[MAXL]; //Array di variabili, da usare come "mediatore" fra le variabili globali e i comandi

    //Errore di input
    if (!cmd.var[0] && !cmd.funct[0])
        cmdExceptions(1);
    //Stampe di variabili, procedure particolari (exit, clear screen, help...)
    else if (!cmd.funct[0] && !cmd.arg[0][0])
    {
        v[0] = cercaVar(cmd.var, 0);
        //Help
        if (!strcmp(cmd.var, "help"))
            help();
        //Cancella schermata
        else if (!strcmp(cmd.var, "clear"))
            system(CLEAR);
        //Esci
        else if (!strcmp(cmd.var, "exit"))
            quit();
        else if (v[0] == ERR)
            cmdExceptions(3);
        else
            stampaMatrice(vars[v[0]]);
    }
    
    //Inizializzazione matrice
    else if (!strcmp(cmd.funct, "nuova"))
    {
        v[0] = cercaVar(cmd.arg[0], 1);
        initMatrix(&vars[v[0]], cmd.arg[0], strtol(cmd.arg[1], NULL, 10), strtol(cmd.arg[2], NULL, 10), strtod(cmd.arg[3], NULL), strtol(cmd.arg[4], NULL, 10));
        if (cmd.print)
            stampaMatrice(vars[v[0]]);
    }
    //Inizializzazione di una matrice da terminale
    else if (cmd.funct[0] == '[')
    {
        char str[MAXL];
        strcpy(str, cmd.var);
        strcat(str, " = ");
        strcat(str, cmd.funct);
        strcat(str, "\n");
        v[0] = cercaVar(cmd.var, 1);
        getStringMatrix(str, &vars[v[0]]);
        if (cmd.print)
            stampaMatrice(vars[v[0]]);
    }
    //Prende n matrici dal file di testo "matrice.txt". Senza parametri le prende tutte
    else if (!strcmp(cmd.funct, "getFileMatrix"))
    {
        FILE *ft = openFile("matrice.txt", "rt");
        Matrice temp;
        int i, n = strtol(cmd.arg[0], NULL, 0);

        if (!cmd.arg[0][0])
            n = MAXV;
        for (i = 0; i < n && !feof(ft); i++)
        {
            getFileMatrix(ft, &temp);
            v[i] = cercaVar(temp.nome, 1);
            copiaMatrice(temp, &vars[v[i]]);
            delMatrix(&temp);
            saveMatrix(&vars[v[i]], vars[v[i]].nome);
        }
        fclose(ft);
    }
    //Eliminazione di una matrice
    else if (!strcmp(cmd.funct, "elimina"))
    {
        v[0] = cercaVar(cmd.arg[0], 0);
        if (v[0] == ERR)
            cmdExceptions(4);
        else
            delMatrix(&vars[v[0]]);
    }
    //Prodotto tra matrici o prodotto per uno scalare
    else if (!strcmp(cmd.funct, "prodotto") || !strcmp(cmd.funct, "*") || !strcmp(cmd.funct, "prodottoSc"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else
        {
            if (vars[v[1]].colonne == 1 && vars[v[1]].righe == 1)
            {
                vars[v[0]] = prodottoSc(vars[v[2]], vars[v[1]].elemento[0][0]);
                if (cmd.var[0])
                    saveMatrix(&vars[v[0]], cmd.var);
            }
            else if (vars[v[2]].colonne == 1 && vars[v[2]].righe == 1)
            {
                vars[v[0]] = prodottoSc(vars[v[1]], vars[v[2]].elemento[0][0]);
                if (cmd.var[0])
                    saveMatrix(&vars[v[0]], cmd.var);
            }
            else
            {
                vars[v[0]] = prodotto(vars[v[1]], vars[v[2]]);
                if (cmd.var[0])
                    saveMatrix(&vars[v[0]], cmd.var);
            }
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
            if (!cmd.var[0])
                delMatrix(&vars[v[0]]);
        }
    }
    //Somma di due matrici
    else if (!strcmp(cmd.funct, "somma") || !strcmp(cmd.funct, "+") || !strcmp(cmd.funct, "-"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else
        {
            if (!strcmp(cmd.funct, "-"))
                vars[v[0]] = somma(vars[v[1]], prodottoSc(vars[v[2]], -1));
            else
                vars[v[0]] = somma(vars[v[1]], vars[v[2]]);
            if (cmd.var[0])
                saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
            if (!cmd.var[0])
                delMatrix(&vars[v[0]]);
        }
    }
    //Trasposta (sia stampa che assegnazione)
    else if (!strcmp(cmd.funct, "trasposta") || !strcmp(cmd.funct, "\'"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.arg[0][0])
        {
            vars[v[0]] = trasposta(vars[v[1]]);
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(trasposta(vars[v[0]]));
    }
    //Determinante
    else if (!strcmp(cmd.funct, "det"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            initMatrix(&vars[v[0]], cmd.var, 1, 1, det(vars[v[1]]), 0);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
        {
            Matrice temp;
            char nome[MAXL];

            strcpy(nome, "det(");
            strcat(nome, vars[v[1]].nome);
            strcat(nome, ")");
            initMatrix(&temp, nome, 1, 1, det(vars[v[1]]), 0);
            stampaMatrice(temp);
            delMatrix(&temp);
        }
    }
    //Traccia della matrice
    else if (!strcmp(cmd.funct, "traccia"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            initMatrix(&vars[v[0]], cmd.var, 1, 1, traccia(vars[v[1]]), 0);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
        {
            Matrice temp;
            char nome[MAXL];

            strcpy(nome, "traccia(");
            strcat(nome, vars[v[1]].nome);
            strcat(nome, ")");
            initMatrix(&temp, nome, 1, 1, traccia(vars[v[1]]), 0);
            stampaMatrice(temp);
            delMatrix(&temp);
        }
    }
    //Matrice inversa
    else if (!strcmp(cmd.funct, "inv"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            vars[v[0]] = inversa(vars[v[1]]);
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(inversa(vars[v[1]]));
    }
    //Matrice aggiunta - complemento algebrico
    else if (!strcmp(cmd.funct, "adj"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            vars[v[0]] = adj(vars[v[1]]);
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(adj(vars[v[1]]));
    }
    //Passsaggio di base dalla base canonica
    else if (!strcmp(cmd.funct, "cambiaBaseC"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            vars[v[0]] = cambiaBaseC(vars[v[1]], vars[v[2]], vars[v[3]]);
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(cambiaBaseC(vars[v[1]], vars[v[2]], vars[v[3]]));
    }
    //Passaggio di base da una base qualsiasi
    else if (!strcmp(cmd.funct, "cambiaBase"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            vars[v[0]] = cambiaBase(vars[v[1]], vars[v[2]], vars[v[3]], vars[v[4]], vars[v[5]]);
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(cambiaBase(vars[v[1]], vars[v[2]], vars[v[3]], vars[v[4]], vars[v[5]]));
    }
    //Rango della matrice
    else if (!strcmp(cmd.funct, "rango"))
    {
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            initMatrix(&vars[v[0]], cmd.var, 1, 1, rango(vars[v[1]]), 0);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
        {
            Matrice temp;
            char nome[MAXL];

            strcpy(nome, "rango(");
            strcat(nome, vars[v[1]].nome);
            strcat(nome, ")");
            initMatrix(&temp, nome, 1, 1, rango(vars[v[1]]), 0);
            stampaMatrice(temp);
            delMatrix(&temp);
        }
    }
    //Forma quadratica
    else if (!strcmp(cmd.funct, "fQ"))
    {
        int dim;
        if (!estraiVars(cmd, v, 0))
            ;
        else if (cmd.var[0])
        {
            double *temp = fQ(vars[v[1]], &dim);

            initMatrix(&vars[v[0]], cmd.var, 1, dim, 0, 0);
            fVToMatrix(&temp, 1, vars[v[0]].colonne, &vars[v[0]]);
            if (cmd.print)
                stampaFQ(vars[v[1]], temp);
        }
        else
            stampaFQ(vars[v[1]], fQ(vars[v[1]], &dim));
    }
    //Reshape della matrice
    else if (!strcmp(cmd.funct, "reshape"))
    {
        v[0] = cercaVar(cmd.var, 1);
        v[1] = cercaVar(cmd.arg[0], 0);
        if (v[1] == ERR || !cmd.arg[1][0] || !cmd.arg[2][0])
            cmdExceptions(2);
        else if (cmd.var[0])
        {
            vars[v[0]] = reshape(vars[v[1]], strtol(cmd.arg[1], NULL, 0), strtol(cmd.arg[2], NULL, 0));
            saveMatrix(&vars[v[0]], cmd.var);
            if (cmd.print)
                stampaMatrice(vars[v[0]]);
        }
        else
            stampaMatrice(reshape(vars[v[1]], strtol(cmd.arg[1], NULL, 0), strtol(cmd.arg[2], NULL, 0)));
    }
}

int estraiVars(command cmd, int *v, int is_void)
{
    int i = 0, off = 0;

    if (!is_void)
        v[i++] = cercaVar(cmd.var, ++off);
    while (cmd.arg[i - off][0])
    {
        v[i] = cercaVar(cmd.arg[i - off], 0);
        if (v[i] == ERR) //Se non trovo una variabile
            return cmdExceptions(2);
        i++;
    }
    return 1;
}

int cercaVar(char var[MAXL], int is_new)
{
    int i;

    for (i = 0; i < v_dim; i++)
        if (!strcmp(vars[i].nome, var))
            return i;
    if (!is_new)
        return ERR;
    for (i = 0; i < v_dim; i++)
        if (!vars[i].nome[0])
        {
            strcpy(vars[v_dim].nome, var);
            return i;
        }
    strcpy(vars[v_dim].nome, var);
    v_dim++;

    return v_dim - 1;
}

char cercaOperatore(char *input)
{
    int i;

    for (i = 0; input[i] != 0; i++)
        if (input[i] == '+' || input[i] == '-' || input[i] == '*' || input[i] == '=' || input[i] == '(' || input[i] == ')' || input[i] == '[' || input[i] == ',' || input[i] == '\'' || input[i] == ';')
            return input[i];
    return 0;
}

int cmdExceptions(int n)
{
    switch (n)
    {
    case 1:
        printf("Input non valido\n");
        break;
    case 2:
        printf("Almeno una variabile non esiste.\n");
        break;
    case 3:
        printf("Il comando o la variabile non esistono! Digitare \'help\' per mostrare la guida\n");
        break;
    case 4:
        printf("Variabile inesistente o già eliminata.\n");
    default:
        printf("Errore non definito\n");
        break;
    }
    return 0;
}

char elaboraInput(char *out, char **input, char op)
{
    int i;

    for (i = 0; (*input)[i] != ' ' && (*input)[i] != op; i++)
        out[i] = (*input)[i];
    out[i] = 0;
    spostaStringa(input, i);

    return cercaOperatore(*input);
}