#include "sortlib.h"
#include "baselib.h"
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include INTRIN_H

#define QSORT_THRESHOLD 64
#define QSORT_COLLAPSE_THRESHOLD 64
#define QSORT_NONBRANCH_THRESHOLD 64
#define QSORT_AVX_THRESHOLD 64
#define QSORT_AVX512_THRESHOLD 64
#define QSORT_COUNTSORT_THRESHOLD (1 << 14)

//#define INSSORT_AVX

int *insertionSort(int *v, int numel)
{
    //Ciclo su tutto il vettore
    for (int i = 1; i < numel; ++i)
        if (v[i] < v[i - 1]) //Confronto la parte ordinata con il nuovo elemento
        {
            int temp = v[i];
            int j = 0;

            while (v[j] < v[i]) //Troviamo il primo elemento maggiore
                ++j;
            for (int k = i; k > j; --k)
                v[k] = v[k - 1];
            v[j] = temp;
        }

    return v;
}

int *insertionSortAVX(int *v, int numel)
{
    //Ciclo su tutto il vettore
    for (int i = 1; i < numel; ++i)
        if (v[i] < v[i - 1]) //Confronto la parte ordinata con il nuovo elemento
        {
            int temp = v[i];
            int j = 0;

            __m512i v_i = _mm512_set1_epi32(temp);

            for (;; j += 16)
            {
                __m512i v_j = _mm512_loadu_si512(v + j);
                __mmask16 msk = _mm512_cmplt_epi32_mask(v_j, v_i);

                if (_popcnt32(msk) < 16)
                    break;
            }
            while (v[j] < v[i]) //Troviamo il primo elemento maggiore
                ++j;

            int k = i - 16;
            for (; k > j; k -= 16) //Facciamo spazio, spostando a destra il vettore
            {
                __m512i v_k = _mm512_loadu_si512(v + k);

                _mm512_storeu_si512(v + k + 1, v_k);
            }
            // elementi rimanenti, risolviamo scalarmente
            for (k += 16; k > j; --k)
                v[k] = v[k - 1];
            v[j] = temp;
        }

    return v;
}


int *bubbleSort(int *v, int numel)
{
    for (int i = 0; i < numel; ++i)
        for (int j = 1, k = 0; j < numel; ++j, ++k)
            if (v[j] < v[k])
            {
                int temp = v[j];

                v[j] = v[k];
                v[k] = temp;
            }

    return v;
}

int *selectionSort(int *v, int numel)
{
    for (int i = 0, last = numel >> 1; i < last; ++i)
    {
        int t;
        int min = i;
        int max = --numel;

        // usiamo due cicli diversi, e partiamo sempre da "i" -> istruzioni vettoriali e loop fusion
        for (int j = i; j <= numel; ++j)
            if (v[j] < v[min])
                min = j;

        for (int j = i; j <= numel; ++j)
            if (v[max] < v[j])
                max = j;

        if (max == i)
            max = min;

        t = v[min];
        v[min] = v[i];
        v[i] = t;
        t = v[max];
        v[max] = v[numel];
        v[numel] = t;
    }

    return v;
}

long *selectionSortl(long *v, int numel)
{
    for (int i = 0, last = numel >> 1; i < last; ++i)
    {
        int min = i;
        int max = --numel;
        long t;

        // usiamo due cicli diversi, e partiamo sempre da "i" -> istruzioni vettoriali e loop fusion
        for (int j = i; j <= numel; ++j)
            if (v[j] < v[min])
                min = j;
        for (int j = i; j <= numel; ++j)
            if (v[max] < v[j])
                max = j;

        t = v[min];
        v[min] = v[i];
        v[i] = t;

        if (max == i)
            max = min;

        t = v[max];
        v[max] = v[numel];
        v[numel] = t;
    }

    return v;
}

int orderedBinarySearch(int *v, int numel, int n)
{
    int hdim = numel >> 1;

    if (!numel || (numel == 1 && v[0] != n))
        return -1;
    if (v[hdim] == n)
        return hdim;

    if (n > v[hdim]) // bisognerebbe farla tail-recursive...
    {
        int out = orderedBinarySearch(v + hdim + 1, hdim - 1 + (numel & 1), n);

        return out >= 0 ? hdim + 1 + out : -1;
    }

    int out = orderedBinarySearch(v, numel + 1 - hdim - (numel & 1), n);

    return out >= 0 ? out : -1;
}

static int *merge(int *v, size_t numel)
{
    size_t hdim = numel >> 1;
    size_t rdim = numel & 1;
    size_t ndim1 = numel - hdim - rdim;
    size_t ndim2 = hdim + rdim;
    int *v1 = (int *)malloc(ndim1 * sizeof *v1);
    int *v2 = (int *)malloc(ndim2 * sizeof *v2);

    if (ndim1 <= ndim2)
    {
        for (size_t i = 0; i < ndim1; ++i)
        {
            v1[i] = v[i];
            v2[i] = v[i + ndim1];
        }
        for (size_t i = ndim1; i < ndim2; ++i)
            v2[i] = v[i + ndim1];
    }
    else
    {
        for (size_t i = 0; i < ndim2; ++i)
        {
            v1[i] = v[i];
            v2[i] = v[i + ndim1];
        }
        for (size_t i = ndim2; i < ndim1; ++i)
            v1[i] = v[i + ndim1];
    }
    for (size_t i = 0, j = 0, k = 0; i < numel; ++i)
    {
        if (k != ndim1 && (v1[k] < v2[j] || j == ndim2))
        {
            int temp = v[i];

            v[i] = v1[k];
            v1[k++] = temp;
        }
        else
        {
            int temp = v[i];

            v[i] = v2[j];
            v2[j++] = temp;
        }
    }
    free(v1);
    free(v2);

    return v;
}

int *mergeSort(int *v, size_t numel)
{
    size_t hdim = numel >> 1;
    size_t rdim = numel & 1;
    size_t ndim1 = numel - hdim - rdim;
    size_t ndim2 = hdim + rdim;

    if (ndim1 > 1)
        mergeSort(v, ndim1);
    if (ndim2 > 1)
        mergeSort(&v[hdim], ndim2);

    return merge(v, numel);
}

int *hMergeSort(int *v, size_t numel)
{
    if (numel < 135)
        return insertionSort(v, (int)numel);

    size_t hdim = numel >> 1;
    size_t rdim = numel & 1;
    size_t ndim1 = numel - hdim - rdim;
    size_t ndim2 = hdim + rdim;

    if (ndim1 > 1)
        hMergeSort(v, ndim1);
    if (ndim2 > 1)
        hMergeSort(&v[hdim], ndim2);

    return merge(v, numel);
}

static int calcolaDim(int *v, int dim, int *average)
{
    int ndim1 = 0;

    do
    {
        int tempAv = 0, flag = 1;

        for (int i = 0, pivot = v[*average]; i < dim; i++)
            if (v[i] < pivot)
                ndim1++;
            else if (!ndim1 && v[i] > pivot && flag)
            {
                tempAv = i;
                flag = 0;
            }
        if (!ndim1)
            *average = tempAv;
    } while (!ndim1);

    return ndim1;
}

static void riempiPartialArray(int *v, int dim, int ndim1, int average)
{
    int pivot = v[average];

    for (int i = 0, j = ndim1; i < ndim1 && j < dim; i++)
        if (v[i] >= pivot)
        {
            int temp;

            while (v[j] >= pivot && j < dim)
                j++;

            temp = v[j];
            v[j] = v[i];
            v[i] = temp;
            j++;
        }
}

/* 
* Una versione molto inefficiente di QuickSort
*/
void ordinaRicorsivo(int *v, int dim)
{
    if (!arrayCheckOrder(v, dim))
    {
        int *v1, *v2;
        int average = dim / 2;
        int ndim1 = calcolaDim(v, dim, &average);
        int ndim2 = dim - ndim1;

        riempiPartialArray(v, dim, ndim1, average);

        v1 = v;
        ordinaRicorsivo(v1, ndim1);

        v2 = v + ndim1;
        ordinaRicorsivo(v2, ndim2);
    }
}

int *countSort(int *v, int dim, int *sv, int sdim, int min)
{
    for (int i = 0; i < dim; ++i)
        ++sv[v[i] - min];

    if (min < 0)
        min = -min;

    for (int i = 0, j = 0; i < sdim; ++i)
        for (int k = 0; k < sv[i]; ++k)
            v[j++] = i + min;

    return v;
}

int *countSortAVX(int *v, int dim, int *sv, int sdim, int min)
{
    for (int i = 0; i < dim; ++i)
        ++sv[v[i] - min];

    if (min < 0)
        min = -min;

    __m256i min_v = _mm256_set1_epi32(min);
    __m256i inc_v = _mm256_set1_epi32(1);

    for (int i = 0, j = 0; i < sdim; ++i)
    {
        int sz = sv[i] - (sv[i] % 8);

        for (int k = 0; k < sz; k += 8, j += 8)
            _mm256_storeu_si256((__m256i *)(v + j), min_v);

        for (int k = sz; k < sv[i]; ++k)
            v[j++] = i + min;
        min_v = _mm256_add_epi32(min_v, inc_v);
    }

    return v;
}

int *countSortAVX512(int *v, int dim, int *sv, int sdim, int min)
{
    for (int i = 0; i < dim; ++i)
        ++sv[v[i] - min];

    if (min < 0)
        min = -min;

    __m512i min_v = _mm512_set1_epi32(min);
    __m512i inc_v = _mm512_set1_epi32(1);

    for (int i = 0, j = 0; i < sdim; ++i)
    {
        int sz = sv[i] - (sv[i] % 16);

        for (int k = 0; k < sz; k += 16, j += 16)
            _mm512_storeu_si512(v + j, min_v);

        for (int k = sz; k < sv[i]; ++k)
            v[j++] = min;
        ++min;
        min_v = _mm512_add_epi32(min_v, inc_v);
    }

    return v;
}

size_t binarySearch(int *v, size_t numel, int n)
{
    return orderedBinarySearch(sort(v, numel), numel, n);
}

int selRNG(int *out, int *allow, int adim)
{
    int temp;

    temp = rand() % adim;
    *out = allow[temp];

    return temp;
}

void shuffle(int *v, int dim)
{
    int *v_out = (int *)malloc(dim * sizeof *v_out), *allow = (int *)malloc(dim * sizeof *allow),
        adim = dim;

    srand(time(NULL));
    for (int i = 0; i < adim; ++i)
        allow[i] = i;
    for (int i = 0; i < dim; ++i, --adim)
    {
        int temp;
        int i_temp = selRNG(&temp, allow, adim);
        allow[i_temp] = allow[adim - 1];
        v_out[i] = v[temp];
    }
    memcpy(v, v_out, sizeof *v * dim);
    free(v_out);
    free(allow);
}

static int partition(int *v, int numel)
{
    int k = rand() % numel;
    int piv = v[k];
    int j = 0;

    /* 
    * Dai test, questo ciclo si è rivelato decisamente più veloce di un ciclo "collapse-the-walls"
    * come quello in Glib qsort(). Questo probabilmente perché, anche se richiede più swap, usa meno 
    * variabili, meno controlli e ha maggiore località (non salta a caso da sinistra a destra),
    * nonostante resti comunque un algoritmo in-place. Inoltre si presta ad essere vettorizzato.
    */
    for (int i = 0; i < numel; ++i)
        if (v[i] < piv)
        {
            int t = v[i];
            v[i] = v[j];
            v[j] = t;
            ++j;
        }

    if (j < k) // swap del pivot
    {
        int t = v[k];

        v[k] = v[j];
        v[j++] = t;
    }

    return j;
}

void quickSort(int *v, int numel)
{
    if (numel <= QSORT_THRESHOLD)
        insertionSort(v, numel);
    else
    {
        int newel = partition(v, numel);

        quickSort(v, newel);
        quickSort(v + newel, numel - newel);
    }
}

static int partitionNonBranch(int *v, int numel)
{
    int k = rand() % numel;
    int piv = v[k];
    int j = 0;

    /* 
    * Dai test, questo ciclo si è rivelato decisamente più veloce di un ciclo "collapse-the-walls"
    * come quello in Glib qsort(). Questo probabilmente perché, anche se richiede più swap, usa meno 
    * variabili, meno controlli e ha maggiore località (non salta a caso da sinistra a destra),
    * nonostante resti comunque un algoritmo in-place. Inoltre si presta ad essere vettorizzato.
    * Inoltre, sostituiamo il costrutto:
    * if(v[i] < piv)
    *   swap(v[i], v[j++]);
    * con una versione non-branching, che dovrebbe essere più veloce.
    */
    for (int i = 0; i < numel; ++i)
    {
        int c = v[i] < piv;
        int t = v[i];

        v[i] = v[j] * c | v[i] * !c;
        v[j] = t * c | v[j] * !c;
        j += c;
    }

    if (j < k) // swap del pivot
    {
        int t = v[k];

        v[k] = v[j];
        v[j++] = t;
    }

    return j;
}

void quickSortNonBranch(int *v, int numel)
{
    if (numel <= QSORT_NONBRANCH_THRESHOLD)
        insertionSort(v, numel);
    else
    {
        int newel = partitionNonBranch(v, numel);

        quickSortNonBranch(v, newel);
        quickSortNonBranch(v + newel, numel - newel);
    }
}

static int *partitionCollapse(int *l, int *r)
{

    int *v = l;
    int piv = *(l + (rand() % (r - l)));

    // Usiamo la tecnica "collapse the walls"
    --r;
    while (l < r)
    {
        // piv esiste per forza, non serve bound-checking
        while (*l < piv)
            ++l;
        while (r > l && *r >= piv)
            --r;

        if (l < r)
        {
            int t = *l;
            *l = *r;
            *r = t;
        }
    }

    return l + (l == v);
}

static void quickSortCollapseRec(int *l, int *r)
{
    if ((r - l) <= QSORT_COLLAPSE_THRESHOLD)
        insertionSort(l, r - l);
    else
    {
        int *m = partitionCollapse(l, r);

        quickSortCollapseRec(l, m);
        quickSortCollapseRec(m, r);
    }
}

void quickSortCollapse(int *v, int numel)
{
    quickSortCollapseRec(v, v + numel);
}


static int *partitionAVX(int *v, int numel)
{
    int *i = v;
    int *last = v + numel;
    int *k = v + (numel >> 1);
    int piv = *k;
    __m256i piv_avx = _mm256_set1_epi32(piv); // Replico il pivot

    for (; i - v < 8 && i < last; ++i) // Devo evitare l'aliasing di v_i e v_j nel prossimo ciclo
        if (piv > *i)
        {
            int t = *i;

            *i = *v;
            *(v++) = t;
        }

    if (last - i < 8) // Se ci sono meno di 8 elementi, non serve AVX
        goto scalar_step;

    // Se ci arrivo "giusto", non mi devo fermare prima
    for (int *lim = last - 7 * (((last - i) & 7) == 0); i < lim; i += 8)
    {
        // non abbiamo cmpgt_mask, compressstore e mask_expand: dobbiamo emularle
        __m256i v_v = _mm256_loadu_si256((const __m256i *)v);     // contiene v[0:7]
        __m256i v_i = _mm256_loadu_si256((const __m256i *)i);     // contiene v[i:i+7]
        __m256i v_msk = _mm256_cmpgt_epi32(piv_avx, v_i);         // confronto con il pivot
        int msk = _mm256_movemask_ps(_mm256_castsi256_ps(v_msk)); // comprimo la maschera
        int inc = _popcnt32(msk);                                 // conto i minori del pivot
        static const int mask_a[][8] = {
            {0, 0, 0, 0, 0, 0, 0, 0},         {~0, 0, 0, 0, 0, 0, 0, 0},
            {~0, ~0, 0, 0, 0, 0, 0, 0},       {~0, ~0, ~0, 0, 0, 0, 0, 0},
            {~0, ~0, ~0, ~0, 0, 0, 0, 0},     {~0, ~0, ~0, ~0, ~0, 0, 0, 0},
            {~0, ~0, ~0, ~0, ~0, ~0, 0, 0},   {~0, ~0, ~0, ~0, ~0, ~0, ~0, 0},
            {~0, ~0, ~0, ~0, ~0, ~0, ~0, ~0},
        };
        __m256i i_msk = _mm256_load_si256((const __m256i *)mask_a[inc]);
        // Mappo i bit in un byte nei byte in un 64bit, poi riempio i byte attivi (0xFF)
        uint64_t pmsk = _pdep_u64(msk, 0x0101010101010101) * 0xFF;
        // emulo mask_compress
        uint64_t pmute = _pext_u64(0x0706050403020100, pmsk);

        v_i = _mm256_permutevar8x32_epi32(v_i, _mm256_cvtepu8_epi32(_mm_cvtsi64_si128(pmute)));
        _mm256_maskstore_epi32(v, i_msk, v_i);

        // emulo mask_expand
        pmute = _pdep_u64(0x0706050403020100, pmsk);
        v_v = _mm256_permutevar8x32_epi32(v_v, _mm256_cvtepu8_epi32(_mm_cvtsi64_si128(pmute)));
        _mm256_maskstore_epi32(i, v_msk, v_v);

        v += inc;
    }

scalar_step:
    for (; i < last; ++i) // Finisco gli ultimi elementi in modo scalare
        if (piv > *i)
        {
            int t = *i;

            *i = *v;
            *(v++) = t;
        }

    if (v < k) // swap del pivot
    {
        int t = *k;

        *k = *v;
        *(v++) = t;
    }

    return v;
}

void quickSortAVX(int *v, int numel)
{
    if (numel < QSORT_AVX_THRESHOLD)
        selectionSort(v, numel);
    else
    {
        int *nv = partitionAVX(v, numel);
        int nleft = (int)(nv - v);
        int nright = (int)(v + numel - nv);

        quickSortAVX(v, nleft);
        quickSortAVX(nv, nright);
    }
}

#if 0
static long *AVXpartitionl(long *v, int numel)
{
    long *i = v;
    long *last = v + numel;
    long *k = v + (rand() & (numel - 1));
    long piv = *k;
    __m256i piv_avx = _mm256_set1_epi64x(piv); // Replico il pivot

    // Devo evitare l'aliasing di v_i e v_j nel prossimo ciclo
    for (; i - v < 4 && i < last; ++i)
        if (piv > *i)
        {
            long t = *i;

            *i = *v;
            *(v++) = t;
        }

    // Se ci arrivo "giusto", non mi devo fermare prima
    for (long *lim = (_tzcnt_u64(last - i) > 1) ? last : last - 3; i < lim; i += 4) // Parte AVX
    {
        // non abbiamo cmpgt_mask, compressstore e mask_expand: dobbiamo emularle
        __m256i v_v = _mm256_load_si256((const __m256i *)v);      // v[0:3]
        __m256i v_i = _mm256_load_si256((const __m256i *)i);      //v[i:i+3]
        __m256i v_msk = _mm256_cmpgt_epi64(piv_avx, v_i);         // confronto con il pivot
        int msk = _mm256_movemask_pd(_mm256_castsi256_pd(v_msk)); // comprimo la maschera
        int inc = _popcnt32(msk);                                 // conto i minori del pivot
        static const long mask_a[][4] = {
            {0, 0, 0, 0},
            {~0, 0, 0, 0},
            {~0, ~0, 0, 0},
            {~0, ~0, ~0, 0},
            {~0, ~0, ~0, ~0},
        };
        __m256i i_msk = _mm256_load_si256((const __m256i *)mask_a[inc]);

        switch (msk)
        {
        // Ci sono 16 permutazioni
        case 0b0010:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00000001);
            v_v = _mm256_permute4x64_epi64(v_v, 0b00000000);
            break;
        case 0b0100:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00000010);
            v_v = _mm256_permute4x64_epi64(v_v, 0b00000000);
            break;
        case 0b0101:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00001000);
            v_v = _mm256_permute4x64_epi64(v_v, 0b00010000);
            break;
        case 0b0110:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00001001);
            v_v = _mm256_permute4x64_epi64(v_v, 0b00010000);
            break;
        case 0b1000:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00000011);
            v_v = _mm256_permute4x64_epi64(v_v, 0b00000000);
            break;
        case 0b1001:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00001100);
            v_v = _mm256_permute4x64_epi64(v_v, 0b01000000);
            break;
        case 0b1010:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00001101);
            v_v = _mm256_permute4x64_epi64(v_v, 0b01000000);
            break;
        case 0b1011:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00110100);
            v_v = _mm256_permute4x64_epi64(v_v, 0b10000100);
            break;
        case 0b1100:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00001110);
            v_v = _mm256_permute4x64_epi64(v_v, 0b01000000);
            break;
        case 0b1101:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00111000);
            v_v = _mm256_permute4x64_epi64(v_v, 0b10010000);
            break;
        case 0b1110:
            v_i = _mm256_permute4x64_epi64(v_i, 0b00111001);
            v_v = _mm256_permute4x64_epi64(v_v, 0b10010000);
            break;
        }
        _mm256_maskstore_epi64(v, i_msk, v_i);
        _mm256_maskstore_epi64(i, v_msk, v_v);

        v += inc;
    }

    for (; i < last; ++i) // Finisco gli ultimi elementi in modo scalare
        if (piv > *i)
        {
            long t = *i;

            *i = *v;
            *(v++) = t;
        }

    if (v < k) // swap del pivot
    {
        long t = *k;

        *k = *v;
        *(v++) = t;
    }

    return v;
}
#endif

static int *partitionAVX512(int *l, int *r)
{
    int *i = l;
    int *k = l + (rand() % (r - l));
    int piv = *k;
    __m512i piv_avx = _mm512_set1_epi32(piv); // Replico il pivot

    for (; i - l < 16 && i < r; ++i) // Devo evitare l'aliasing di v_i e v_j nel prossimo ciclo
    {
        int c = *i < piv;
        int t = *i;

        *i = *l * c | *i * !c;
        *l = t * c | *l * !c;
        l += c;
    }

    if (i == r)
    {
        if (l < k) // swap del pivot
        {
            int t = *k;

            *k = *l;
            *(l++) = t;
        }

        return l;
    }

    // Parte AVX512: molto più semplice della versione AVX-2!
    for (int *r_avx = r - 15; i < r_avx; i += 16)
    {
        __m512i v_l = _mm512_loadu_si512(l);                   // carico v[j] in base alla maschera
        __m512i v_i = _mm512_loadu_si512(i);                   // contiene v[i:i+15]
        __mmask16 msk = _mm512_cmpgt_epi32_mask(piv_avx, v_i); // confronto v[i:i+15] con il pivot
        int inc = _popcnt32(msk);                              // conto quanti sono minori del pivot

        // gli elementi di v_i < piv vanno compressi in v[j:j+inc]
        _mm512_mask_compressstoreu_epi32(l, msk, v_i);
        // e quelli in v[j:j+inc] vanno espansi in v[i:i+15]
        v_l = _mm512_mask_expand_epi32(v_i, msk, v_l);
        _mm512_storeu_si512(i, v_l);
        l += inc;
    }

    for (; i < r; ++i) // Finisco di ordinare scalarmente gli ultimi elementi
        if (piv > *i)
        {
            int t = *i;

            *i = *l;
            *(l++) = t;
        }

    if (l < k) // swap del pivot
    {
        int t = *k;

        *k = *l;
        *(l++) = t;
    }

    return l;
}

static void quickSortAVX512Rec(int *l, int *r)
{
    if (r - l < QSORT_AVX512_THRESHOLD)
        insertionSort(l, r - l);
    else
    {
        int *m = partitionAVX512(l, r);

        quickSortAVX512Rec(l, m);
        quickSortAVX512Rec(m, r);
    }
}

void quickSortAVX512(int *v, int numel)
{
    quickSortAVX512Rec(v, v + numel);
}

#if 0

void AVXSortl(long *v, int numel)
{
    if (numel < QSORT_AVXTHRESHOLD)
        selectionSortl(v, numel);
    else
    {
        long *nv = AVXpartitionl(v, numel);
        int nleft = (int)(nv - v);
        int nright = (int)(v + numel - nv);

        AVXSortl(v, nleft);
        AVXSortl(nv, nright);
    }
}
#endif

void bitSort(void *v, size_t sz, size_t numel)
{
    // we use at most 2^16 bytes of additional memory, but this is arbitrary!

    size_t sv[0x10000] = {0};
    size_t chunks = sz / 2;
    size_t rem = sz % 2;
    void *w = malloc(numel * sz);

    for (size_t j = 0; j < chunks; ++j)
    {
        void *t;
        // count buckets
        for (size_t i = 0; i < numel; ++i)
        {
            uint16_t d = *(uint16_t *)((uint8_t *)v + i * sz + j * 2);
            ++sv[d];
        }

        // compute bucket offsets
        for (size_t i = 0, s = 0; i < 0x10000U; ++i)
        {
            size_t t = sv[i];

            sv[i] = s;
            s += t;
        }

        // fill buckets
        for (size_t i = 0; i < numel; ++i)
        {
            uint16_t d = *(uint16_t *)((uint8_t *)v + i * sz + j * 2);
            memcpy((char *)w + sv[d]++ * sz, (uint8_t *)v + i * sz, sz);
        }

        // repeat with swapped arrays for the MSBs
        memset(sv, 0, sizeof(sv));

        t = v;
        v = w;
        w = t;
    }

    if (rem)
    {
        void *t;

        // count buckets
        for (size_t i = 0; i < numel; ++i)
        {
            uint8_t d = *((uint8_t *)v + i * sz + chunks * 2);
            ++sv[d];
        }

        // compute bucket offsets
        for (size_t i = 0, s = 0; i < 0x100U; ++i)
        {
            size_t t = sv[i];

            sv[i] = s;
            s += t;
        }

        // fill buckets
        for (size_t i = 0; i < numel; ++i)
        {
            uint8_t d = *((uint8_t *)v + i * sz + chunks * 2);
            memcpy((char *)w + sv[d]++ * sz, (char *)v + i * sz, sz);
        }


        t = v;
        v = w;
        w = t;
    }

    // erase the correct pointer, and copy memory if necessary
    chunks += rem;
    if (chunks & 1)
    {
        // w points to the original data, and the correct data is in v
        memcpy(w, v, numel * sz);
        free(v);
    }
    else
    {
        // v points to the original data, and it also contains the correct data
        free(w);
    }
}

int *sort(int *v, int numel)
{
#if defined(__AVX512__)
    return sortAVX512(v, numel);
#elif defined(__AVX2__)
    return sortAVX(v, numel);
#else
    return sortNoAVX(v, numel);
#endif
}

int *sortNoAVX(int *v, int numel)
{
    int2_t mm = arrayMinMax(v, numel);
    int sdim = mm.y - mm.x + 1;

    if (numel < 2 || sdim < 2)
        return v;

    if (sdim > numel)
        quickSort(v, numel);
    else
    {
        int *sv = (int *)calloc(sdim, sizeof *sv);

        countSort(v, numel, sv, sdim, mm.x);
        free(sv);
    }

    return v;
}

int *sortAVX(int *v, int numel)
{
    int2_t mm = arrayMinMax(v, numel);
    int sdim = mm.y - mm.x + 1;

    if (numel < 2 || sdim < 2)
        return v;

    if (sdim > numel)
        quickSortAVX(v, numel);
    else
    {
        int *sv = (int *)calloc(sdim, sizeof *sv);

        countSortAVX(v, numel, sv, sdim, mm.x);
        free(sv);
    }

    return v;
}

int *sortAVX512(int *v, int numel)
{
    int2_t mm = arrayMinMax(v, numel);
    int sdim = mm.y - mm.x + 1;

    if (numel < 2 || sdim < 2)
        return v;

    if (sdim > numel)
        quickSortAVX512(v, numel);
    else
    {
        int *sv = (int *)calloc(sdim, sizeof *sv);

        countSortAVX512(v, numel, sv, sdim, mm.x);
        free(sv);
    }

    return v;
}

void DisordinaVettore(int *v, int dim, int grado)
{
    int n_swap = dim * grado / 100;

    for (int i = 0; i < dim - 1 && i < n_swap; i++)
        for (int j = 0; j < n_swap; j++)
            if (v[i] < v[i + 1])
            {
                int temp = v[i];
                v[i] = v[j];
                v[i + 1] = temp;
            }
}

int cmpInt(const void *a, const void *b)
{
    const int ia = *(const int *)a, ib = *(const int *)b;

    return (ia > ib) - (ib > ia);
}
