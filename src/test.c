#include "baselib.h"
#include "chronolib.h"
#include <stdlib.h>
#include <string.h>

static void benchmark(size_t sz, int tests)
{
    char *v = malloc(sz);
    char *w = malloc(sz);

    chrono_measure(memcpy(v, w, sz), tests);
    chrono_measure(memcpyAVX(v, w, sz), tests);

    free(v);
    free(w);
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        fputs("Sintassi: test.exe <size> <tests>", stderr);
        exit(EXIT_FAILURE);
    }

    benchmark(strtoul(argv[1], NULL, 0), strtoul(argv[2], NULL, 0));

    return 0;
}
