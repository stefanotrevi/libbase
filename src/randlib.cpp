#include "randlib.h"

#include <random>


static std::mt19937_64 rng;

void randomSeed(uint64_t x)
{
    rng.seed(x != RANDOM_ANYSEED ? x : std::random_device{}());
}

float randomF(float low, float upp)
{
    return std::uniform_real_distribution<float>{low, upp}(rng);
}

double randomD(double low, double upp)
{
    return std::uniform_real_distribution<double>{low, upp}(rng);
}

uint32_t random32(uint32_t low, uint32_t upp)
{
    return std::uniform_int_distribution<uint32_t>{low, upp}(rng);
}

uint64_t random64(uint64_t low, uint64_t upp)
{
    return std::uniform_int_distribution<uint64_t>{low, upp}(rng);
}
