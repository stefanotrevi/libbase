#include "heaplib.h"

#ifdef _WIN32
    #define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdlib.h>

int heapLeft(heap_t h, int i)
{
    size_t l = (i << 1) + 1;

    return l < h.size ? l : 0;
}

int heapRight(heap_t h, int i)
{
    size_t r = (i << 1) + 2;

    return r < h.size ? r : 0;
}

int heapParent(heap_t h, int i)
{
    return i ? (i - 1) >> 1 : 0;
}

void heapMinify(heap_t *h, int i)
{
    int min = i, l = heapLeft(*h, i), r = heapRight(*h, i);

    if (l && h->element[l] < h->element[i])
        min = l;
    if (r && h->element[r] < h->element[min])
        min = r;

    if (min != i)
    {
        int temp = h->element[i];

        h->element[i] = h->element[min];
        h->element[min] = temp;
        heapMinify(h, min);
    }
}

void heapBuildMin(heap_t *h)
{
    int i;

    h->size = h->max_size;
    for (i = heapParent(*h, h->size); i >= 0; --i)
        heapMinify(h, i);
}

void heapMaxify(heap_t *h, int i)
{
    int max = i, l = heapLeft(*h, i), r = heapRight(*h, i);

    if (l && h->element[l] > h->element[i])
        max = l;
    if (r && h->element[r] > h->element[max])
        max = r;

    if (max != i)
    {
        int temp = h->element[i];

        h->element[i] = h->element[max];
        h->element[max] = temp;
        heapMaxify(h, max);
    }
}

void heapMaxifyIt(heap_t *h, int i)
{
    int max, l, r, flag = 1;

    do
    {
        max = i;
        l = heapLeft(*h, max);
        r = heapRight(*h, max);
        if (l && h->element[l] > h->element[max])
            max = l;
        if (r && h->element[r] > h->element[max])
            max = r;

        if (max != i)
        {
            int temp = h->element[i];
            h->element[i] = h->element[max];
            h->element[max] = temp;
            i = max;
        }
        else
            flag = 0;
    } while (flag);
}

void heapBuildMax(heap_t *h)
{
    h->size = h->max_size;
    for (int i = heapParent(*h, h->size); i >= 0; --i)
        heapMaxifyIt(h, i);
}

heap_t arrayToHeap(int *v, size_t dim)
{
    heap_t h;

    h.max_size = dim;
    h.element = v;

    return h;
}

int *heapSort(int *v, size_t dim)
{
    heap_t h = arrayToHeap(v, dim);

    heapBuildMax(&h);
    for (int i = h.size - 1; i >= 1; --i)
    {
        int temp = v[i];
        v[i] = v[0];
        v[0] = temp;
        --h.size;
        heapMaxifyIt(&h, 0);
    }

    return v;
}
