#include "bstlib.h"
#include "hashlib.h"
#include "listlib.h"
#include "rbtlib.h"
#include "baselib.h"
#include "setlib.h"

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#include <io.h>
#include <windows.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <fcntl.h>

//Array che contiene i numeri primi, e flag di avvenuta inizializzazione
static int *primes, init_flag = 1;

void setupPrimes()
{
    if (!init_flag)
        return;

    fprintf(stderr, "[INFO] hcInit()): Prima chiamata, inizializzo...\n");
#ifdef _WIN32
    //Eseguo una mappatura del file, è inutile caricare i dati in un array (richiede un sacco di tempo)
    HANDLE prime_file;
    HANDLE map_pf;

    //Apro il file primes.dat
    prime_file = CreateFileA("primes.dat", GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_READONLY, NULL);
    if (prime_file == INVALID_HANDLE_VALUE)
    {
        perror("File non trovato!");
        exit(EXIT_FAILURE);
    }
    //lo mappo in memoria
    map_pf = CreateFileMappingA(prime_file, NULL, PAGE_READONLY, 0, 0, NULL);
    //E ricavo la map_view
    primes = MapViewOfFile(map_pf, FILE_MAP_READ, 0, 0, NPRIME * sizeof(int));
    CloseHandle(prime_file);
    CloseHandle(map_pf);
#else
    int prime_file = open("primes.dat", O_RDONLY);

    if (prime_file == -1)
    {
        perror("primes.dat");
        exit(EXIT_FAILURE);
    }
    primes = mmap(NULL, NPRIME * sizeof(int), PROT_READ, MAP_PRIVATE, prime_file, 0);
    close(prime_file);
#endif
    init_flag = 0;
}

int findPrime(int dim)
{
    // se non basta, fallisco
    if (dim > primes[NPRIME - 1])
        return 0;

    int dim_l2 = ilog2(dim), i;
    // potenze di 2 inf/sup e range
    int bot = 1 << dim_l2, top = 2 << dim_l2, range = top - bot;
    // punto casuale e "lontano" dagli estremi (fra 1/4 e 3/4 del range)
    int point = bot + (range >> 2) + ((rand() % range) >> 1);

    bot = 0;
    top = NPRIME;
    // ricerca binaria per trovare il punto desiderato
    for (i = bot + (top - bot) >> 1; bot != top; i = bot + (top - bot) >> 1)
        if (primes[i] > point)
            top = i;
        else if (primes[i] < point)
            bot = i;
        else
            break;

    return primes[i];
}

void freePrimes(void)
{
#ifdef _WIN32
    UnmapViewOfFile(primes);
#else
    munmap(primes, NPRIME * sizeof(int));
#endif
}

int hcInit(hctable_t *ht, int dim, int repeats, hashkind_t kind)
{
    int i;

    if (!dim || dim >= NPRIME)
    {
        ht->size = 0; // evita errori in caso di chiamata a delete
        return HASH_ERR_SIZE;
    }
    // 0 ripetizioni significa 1 ripetizione probabilmente, correggiamo...
    if (!repeats)
        repeats = 1;
    //Se la dimensione è più piccola delle ripetizioni accettate, non faccio la divisione
    if (dim / repeats)
        dim /= repeats;
    //log(1) = 0, meglio di no, manda in confusione findPrime()
    if (dim == 1)
        dim = 2;
    // prima chiamata, inizializzo i numeri primi
    if (init_flag)
        setupPrimes();

    ht->size = findPrime(dim);

    switch (kind)
    {
    case HASH_LIST:
        ht->table = malloc(ht->size * sizeof(list_t));
        ht->init = (void (*)(chain_t))listInit;
        ht->del = (void (*)(chain_t))listDelete;
        ht->put = (chain_t (*)(chain_t, int))listPut;
        ht->find = (chain_t(*)(chain_t, int))listFind;
        ht->print = (chain_t(*)(chain_t))listPrint;
        break;
    case HASH_BST:
        ht->table = malloc(ht->size * sizeof(bst_t));
        ht->init = (void (*)(chain_t))bstInit;
        ht->del = (void (*)(chain_t))bstDelete;
        ht->put = (chain_t (*)(chain_t, int))bstPut;
        ht->find = (chain_t(*)(chain_t, int))bstFind;
        ht->print = (chain_t(*)(chain_t))bstPrintOrd;
        break;
    case HASH_RBT:
        ht->table = malloc(ht->size * sizeof(rbt_t));
        ht->init = (void (*)(chain_t))rbtInit;
        ht->del = (void (*)(chain_t))rbtDelete;
        ht->put = (chain_t (*)(chain_t, int))rbtPut;
        ht->find = (chain_t(*)(chain_t, int))rbtFind;
        ht->print = (chain_t(*)(chain_t))rbtPrintOrd;
        break;
    default:
        return HASH_ERR_KIND;
    }
    for (i = 0; i < ht->size; i++)
        ht->init(ht->table[i]);

    return 0;
}

int fhash(int mod, int n)
{
    return n % mod;
}

hctable_t *hcPut(hctable_t *ht, int n)
{
    ht->put(ht->table[fhash(ht->size, n)], n);

    return ht;
}

void hcDelete(hctable_t *ht)
{
    int i;

    if (ht->size)
    {
        for (i = 0; i < ht->size; i++)
            ht->del(ht->table[i]);
        ht->size = 0;
        free(ht->table);
    }
}

void hcPrint(hctable_t ht)
{
    int i;

    if (!ht.size)
        printf("[]");
    else
    {
        putchar('[');
        for (i = 0; i < ht.size; i++)
        {
            printf("%d: ", i);
            ht.print(ht.table);
        }
        putchar(']');
    }

    putchar('\n');
}

chain_t hcFind(hctable_t ht, int n)
{
    return ht.find(ht.table[fhash(ht.size, n)], n);
}

void hoInit(hotable_t *ht, int dim, hashkind_t kind)
{
    if (dim > NPRIME)
        perror("La dimensione richiesta è troppo grande!\n");
    else
    {
        if (kind == OHASH_SET)
        {
            ht->getData = (int (*)(void *))setGetData;
            ht->getRep = (void *(*)(void *))setGetRep;
        }
        else if (kind == OHASH_RSET)
        {
            ht->getData = (int (*)(void *))rsetGetData;
            ht->getRep = (void *(*)(void *))rsetGetFather;
        }
        else
            fprintf(stderr, "Tipo di tabella hash non supportato!\n");

        if (init_flag)
        {
            FILE *prime_file = openFile("../librerie/primes.txt", "rt");

            for (int i = 0; i < NPRIME; ++i)
                fscanf(prime_file, "%d", &primes[i]);
            init_flag = 0;
            fclose(prime_file);
        }

        //Devo essere sicuro che riesca a contenere ALMENO "dim" elementi
        ht->size = findPrime(dim);
        ht->numel = 0;
        ht->table = malloc(ht->size * sizeof *ht->table);
        for (int i = 0; i < ht->size; i++)
            ht->table[i].busy = 0;
    }
}

int ofhash(int mod, int n, int i)
{
    return (fhash(mod, n) + i) % mod;
}

int hoPut(hotable_t *ht, void *d)
{
    int h_value;

    if (ht->numel == ht->size)
        return -1;
    //Inserisco gli elementi nel primo slot libero (probing lineare)
    for (int i = 0, index; i <= ht->numel; ++i)
        if (!ht->table[index = ofhash(ht->size, h_value, i)].busy)
        {
            ht->table[index].el = d;
            ht->table[index].busy = 1;
            ++ht->numel;

            return 0;
        }

    return -1; // impossibile (i != ht->numel), ma il compilatore si lamenta...
}

void hoDel(hotable_t *ht)
{
    for (int i = 0; i < ht->size; ++i)
        if (ht->table[i].busy)
            free(ht->table[i].el);
    free(ht->table);
    ht->size = ht->numel = 0;
}

void hoPrint(hotable_t ht)
{
    if (!ht.numel)
        printf("[]");
    else
    {
        putchar('[');
        for (int i = 0, first = 1; i < ht.size; i++)
            if (!ht.table[i].busy)
            {
                printf("%s%d: %d->%d", first ? "" : ", ", i,
                       ht.getData(ht.table[i].el),
                       ht.getData(ht.getRep(ht.table[i].el)));
                first = 0;
            }
        putchar(']');
    }
}

void *hoFind(hotable_t ht, int n)
{
    int index, h_value;

    if (!ht.numel)
        return NULL;
    for (int i = 0; i < ht.numel; i++)
    {
        index = ofhash(ht.size, n, i);
        //Se non è occupato, allora non esiste
        if (!ht.table[index].busy)
            return NULL;
        h_value = ht.getData(ht.table[index].el);
        if (h_value == n)
            return ht.table[index].el;
    }
    return NULL;
}
