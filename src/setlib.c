#include "hashlib.h"
#include "setlib.h"

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static hotable_t setHash; // tabella hash per contenere i puntatori di un Set

set_t *setMake(int n)
{
    set_el_t *e;

    // non si accettano duplicati
    if (e = hoFind(setHash, n))
        return e->set;
    // Definisco un elemento ed un corrispondente set
    set_t *S = malloc(sizeof *S);

    e = malloc(sizeof *e);
    e->data = n;
    e->next = NULL;
    e->set = S;
    S->rep = S->tail = e;
    S->numel = 1;
    // ed inserisco nella tabella hash
    hoPut(&setHash, e);

    return S;
}

set_t *setFind(int d)
{
    set_el_t *e;

    //Sfrutto la tabella hash per recuperare l'elemento in tempo O(1)
    if (!(e = hoFind(setHash, d)))
        return NULL;

    return e->set;
}

set_el_t *setRepFind(int d)
{
    set_el_t *e;

    if (!(e = hoFind(setHash, d)))
        return NULL;

    return e->set->rep;
}

int setUnion(int d1, int d2, int weighted)
{
    set_t *S_in = setFind(d1), *S_out = setFind(d2);
    set_el_t *rep_in, *rep_out;

    //Se non esistono, ritorno un codice di errore
    if (!S_in || !S_out)
        return -1;
    //Se appartengono allo stesso insieme, non faccio nulla
    if (S_in == S_out)
        return 0;

    //Prendo come output il set con più elementi
    if (weighted && S_in->numel > S_out->numel)
    {
        set_t *temp = S_in;

        S_in = S_out;
        S_out = temp;
    }

    //Trovo i rispettivi rappresentanti
    rep_out = S_out->rep;
    rep_in = S_in->rep;
    //Aggiorno la coda ed il numero di elementi
    S_out->tail->next = rep_in;
    S_out->tail = S_in->tail;
    S_out->numel += S_in->numel;

    //Aggiorno il set di appartenenza di ciascun elemento
    while (rep_in)
    {
        rep_in->set = S_out;
        rep_in = rep_in->next;
    }
    //Libero la memoria e aggiorno la tabella hash
    free(S_in);

    return 0;
}

int setUnionMany(int *dv, size_t dim)
{
    //Se la dimensione è nulla, errore
    if (!dim)
        return -1;

    for (size_t i = 1; i < dim; i++)
        setUnion(dv[0], dv[i], 1);

    return 0;
}

set_t *setPrint(int d)
{
    // Trovo il set e salvo il rappresentante
    set_t *S = setFind(d);

    if (!S->numel)
        printf("[]");
    else
    {
        // Stampo tutti gli elementi
        printf("[%d: [", S->rep->data);
        for (set_el_t *r = S->rep; r->next; r = r->next)
            printf("%d, ", r->data);
        printf("%d]]", S->rep->data);
    }

    return S;
}

int setGetData(set_el_t *e)
{
    return e ? e->data : 0;
}

set_el_t *setGetRep(set_el_t *e)
{
    return e ? e->set->rep : NULL;
}

rset_t *rsetMake(int d)
{
    rset_t *e;

    // non si accettano duplicati
    if (e = hoFind(setHash, d))
        return e;

    e = malloc(sizeof *e);
    e->data = d;
    e->father = e; //Il padre del nodo è il nodo stesso
    e->rank = 0;   //Rango
    // Ed inserisco nella tabella hash
    hoPut(&setHash, e);

    return e;
}

rset_t *rsetFind(int d)
{
    rset_t *e;

    if (!(e = hoFind(setHash, d)) || e == e->father)
        return e;

    return e->father = rsetFind(e->father->data);
}

rset_t *rsetLink(rset_t *e1, rset_t *e2)
{
    // i due set erano già stati uniti
    if (e1 == e2)
        return e1;

    if (e1->rank < e2->rank)
    {
        rset_t *temp = e1;
        e1 = e2;
        e2 = temp;
    }
    if (e1->rank == e2->rank)
        ++e1->rank;

    // aggiorno il numero di elementi, e cambio il padre
    return e2->father = e1;
}

rset_t *rsetUnion(int d1, int d2)
{
    rset_t *e1 = rsetFind(d1), *e2 = rsetFind(d2);

    // devono esistere entrambi
    return e1 && e2 ? rsetLink(e1, e2) : NULL;
}

int rsetUnionMany(int *dv, size_t dim)
{
    for (size_t i = 1; i < dim; i++)
        if (!rsetUnion(dv[i - 1], dv[i]))
            return -1;

    return 0;
}

rset_t *rsetPrint(int d)
{
    rset_t *all_father = rsetFind(d);

    if (!all_father)
        printf("[]");
    else
    {

        printf("[%d: [", all_father->data);
        for (int i = 0; i < setHash.size; ++i)
        {
            rset_t *temp = setHash.table[i].el;

            if (temp->father == all_father)
                printf("%d, ", temp->father->data);
        }
        printf("%d]]", all_father->data);
    }

    return all_father;
}

int rsetGetData(rset_t *e)
{
    return e ? e->data : 0;
}

rset_t *rsetGetFather(rset_t *e)
{
    return e ? e->father : NULL;
}
