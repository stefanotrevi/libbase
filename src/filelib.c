#include "filelib.h"

#include <fcntl.h>
#include <stdlib.h>
#ifdef _WIN32
    #include <Windows.h>
    #include <direct.h>
    #include <io.h>
#else
    #include <sys/mman.h>
    #include <sys/sendfile.h>
    #include <sys/stat.h>
    #include <unistd.h>
#endif

int fileExists(const char *fname)
{
    return access(fname, 0) == 0;
}

void *fileMap(const char *fname, size_t size, int readonly)
{
#ifdef _WIN32
    HANDLE f = readonly ? CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                                     FILE_ATTRIBUTE_READONLY | FILE_FLAG_RANDOM_ACCESS, NULL)
                        : CreateFile(fname, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                                     FILE_FLAG_RANDOM_ACCESS, NULL);

    if (f == INVALID_HANDLE_VALUE)
    {
        fprintf(stderr, "Error opening file: %lx\n", GetLastError());

        return NULL;
    }

    HANDLE f_map = readonly ? CreateFileMapping(f, NULL, PAGE_READONLY, 0, 0, NULL)
                            : CreateFileMapping(f, NULL, PAGE_READWRITE, size >> 32, size, NULL);

    if (!f_map)
    {
        CloseHandle(f);
        fprintf(stderr, "Error creating file mapping: %lx\n", GetLastError());

        return NULL;
    }

    void *map = readonly ? MapViewOfFile(f_map, FILE_MAP_READ, 0, 0, size)
                         : MapViewOfFile(f_map, FILE_MAP_ALL_ACCESS, 0, 0, size);

    CloseHandle(f);
    CloseHandle(f_map);

    return map;
#else
    int fd = readonly ? open(fname, O_RDONLY) : open(fname, O_RDWR | O_CREAT);

    if (fd < 0)
        return NULL;

    void *map = readonly ? mmap(map, size, PROT_READ, MAP_SHARED, fd, 0)
                         : mmap(map, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    close(fd);

    if (map == MAP_FAILED)
        return NULL;

    return map;
#endif
}

void fileUnmap(void *map, size_t size)
{
#ifdef _WIN32
    (void)size;
    UnmapViewOfFile(map);
#else
    munmap(map, size);
#endif
}

size_t fileCopy(const char *dst_name, const char *src_name)
{
#ifdef _WIN32
    FILE *dst = fopen(dst_name, "wb");
    FILE *src = fopen(src_name, "rb");
    size_t n = 0;

    while (!feof(src))
    {
        char buf[BUFSIZ];

        n += fread(buf, sizeof *buf, sizeof buf, src);
        fwrite(buf, sizeof *buf, n, dst);
    }
    fclose(dst);
    fclose(src);

    return n;

#else // sendfile è più efficente, esegue la copia in kernel-space

    int dst = open(dst_name, O_BINARY | O_WRONLY);
    int src = open(src_name, O_BINARY | O_RDONLY);
    size_t n = lseek(src, 0, SEEK_END);

    lseek(src, 0, SEEK_SET);
    n = sendfile(dst, src, NULL, n);

    close(dst);
    close(src);

    return n;

#endif
}

int fileMkdir(const char *dname)
{
#ifdef _WIN32
    return mkdir(dname);
#else
    return mkdir(dname, 0755);
#endif
}

size_t fileSize(const char *fname)
{
    int f = open(fname, O_BINARY | O_RDONLY);
    size_t n = 0;

    if (f < 0)
        return 0;

    n = lseek(f, 0, SEEK_END);
    close(f);

    return n;
}
