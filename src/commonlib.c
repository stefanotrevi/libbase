//info nell'header
#include "commonlib.h"

void RNG(int *out, int min, int max, int n, int seed)
{
    int range = max - min + 1, i;

    if (seed)
        srand(seed);
    else
        srand(time(NULL));
    for (i = 0; i < n; i++)
        out[i] = rand() % (range) + min;
}

void fRNG(float *out, float min, float max, int n, int seed)
{
    int i;

    if (seed)
        srand(seed);
    else
        srand(time(NULL));
    for (i = 0; i < n; i++)
        out[i] = (float)(rand() % 100) / ((100 / max) + 0.1);
}

void slittaVettore(int *v, int dim, int pos)
{
    int i;

    for (i = 1; i < dim - pos; i++)
        v[dim - i] = v[dim - i - 1]; //Ogni elemento viene sovrascritto con il suo precedente
}

void ordinaReplace(int *v, int dim)
{
    int i, j, temp;

    //Ciclo su tutto il vettore
    for (i = 1; i < dim; i++)
    {
        //Confronto la parte ordinata con il nuovo elemento, e trovo dove inserirlo
        if (v[i] < v[i - 1])
        {
            for (j = 0; v[j] <= v[i]; j++) //Usiamo il <= per accelerare "slittaVettore"
                ;
            //Se non è ordinato, "Slitto" il vettore
            temp = v[i];
            slittaVettore(v, i + 1, j); //Slitto tutti gli elementi fino a quello attuale
            v[j] = temp;
        }
    }
}

void ordinaRicorsivo(int *v, int dim)
{
    int *v1, *v2, ndim1, ndim2, average = dim / 2;

    if (!checkArray(v, dim))
    {
        if (dim < 50)
            ordinaReplace(v, dim);
        else
        {
            ndim1 = calcolaDim(v, dim, &average);
            ndim2 = dim - ndim1;
            riempiPartialArray(v, dim, ndim1, average);
            v1 = v;
            v2 = v + ndim1;

#ifdef __INTEL_COMPILER
            cilk_spawn ordinaRicorsivo(v1, ndim1);
            ordinaRicorsivo(v2, ndim2);
            cilk_sync;
#else
            ordinaRicorsivo(v1, ndim1);
            ordinaRicorsivo(v2, ndim2);
#endif
        }
    }
}

//Ordina un vettore di interi conoscendo il minimo e il massimo. Se sconosciuti (entrambi = 0), li trova
void countSort(int *v, int dim, int min, int max)
{
    int *sv, i, j = 0, sdim;

    if (!max && !min)
    {
        max = min = v[0];
        for (i = 0; i < dim; i++)
        {
            if (v[i] < min)
                min = v[i];
            if (v[i] > max)
                max = v[i];
        }
    }
    sdim = max - min + 1;
    sv = (int *)malloc(sdim * sizeof(int));
    for (i = 0; i < sdim; i++)
        sv[i] = 0;
    for (i = 0; i < dim; i++)
        sv[v[i] - min]++;
    if (min >= 0)
        for (i = 0; i < sdim; i++)
            while (sv[i])
            {
                v[j] = i + min;
                j++;
                sv[i]--;
            }
    else
        for (i = 0; i < sdim; i++)
            while (sv[i])
            {
                v[j] = i - min;
                j++;
                sv[i]--;
            }
    free(sv);
}

void printArray(int *v, int dim)
{
    int i;

    for (i = 0; i < dim; i++)
        printf("%d ", v[i]);
    printf("\n");
}

void copiaArray(int *dest, int *src, int dim)
{
    int i;

    for (i = 0; i < dim; i++)
        dest[i] = src[i];
}

void fattorizzazione(int a)
{
    int b = 2, i, c, j, f;

    while (b <= sqrt(a))
    {
        i = 0;
        c = a;
        j = 2;
        f = 1;

        while (j < b)
        {
            if (b % j != 0)
            {
                j++;
                j += 2;
            }
            else
            {
                f = 0;
                break;
            }
        }

        while (!(c % b) && f)
        {
            c = c / b;
            i++;
        }

        if (i != 0)
        {
            printf("%d^%d", b, i);
            printf(" x ");
        }

        a = a / pow(b, i);
        b++;
    }
    if (a != 1)
        printf("%d^1\n", a);
}

int potenza(int base, int esp)
{

    int arg, err = 0;
    arg = base;
    if (esp == 0 && base == 0)
        err = 1;
    else if (esp == 0)
        arg = 1;
    for (int i = 1; i < esp; i++)
    {

        arg = arg * base;
    }
    if (err)
        return 0;
    else
        return arg;
}

int numeroContrario(int a)
{

    int b, inv = 0, i = calcolaCifre(a);
    do
    {
        i--;
        b = a % 10;
        a /= 10;
        inv += b * potenza(10, i);
    } while (a);

    return inv;
}

void nscanf(int *buff, int n)
{
    int i, in;
    printf("Inserire %d elementi:\n", n);
    for (i = 0; i < n; i++)
        scanf("%d", &buff[i]);
}

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE **f, int nFile, char **argv)
{
    int i;
    for (i = 0; i < nFile; i++)
    {
        if (f[i] == NULL)
        {
            printf("Il file \"%s\" non esiste.\n", argv[i]);
            exit(-2);
        }
    }
}

FILE *openFile(char *nome, char *modo)
{
    char fName[50];
    FILE *f;

    if (nome)
        f = fopen(nome, modo);
    else
    {

        printf("Inserire nome file:\n");
        scanf("%s", fName);
        f = fopen(fName, modo);
    }

    if (f == NULL)
    {
        if (modo[0] == 'r')
        {
            printf("Impossibile aprire il file (non esiste o è inaccessibile)!\n");
            exit(-1);
        }
        else
        {
            printf("Impossibile creare file (memoria piena o permesso negato)!\n");
            exit(-1);
        }
    }
    return f;
}

void copiaFile(FILE *f1, FILE *f2)
{
    char copy;
    while (!feof(f1))
    {
        fscanf(f1, "%c", &copy);
        fprintf(f2, "%c", copy);
    }
}

FILE *copiaFileMatrice(FILE *f1, FILE *f2, char fName[MAXL])
{
    char copy = 0;
    f2 = fopen(fName, "wt");
    while (copy != ';' && !feof(f1))
    {
        fscanf(f1, "%c", &copy);
        if (copy != ';' && !feof(f1))
            fprintf(f2, "%c", copy);
    }
    fclose(f2);
    f2 = fopen(fName, "rt");
    return f2;
}

void stampaFC(FILE *fT)
{
    char buff[MAXL];

    while (!feof(fT))
    {
        fscanf(fT, "%s", buff);
        if (!tavolaEscape(buff))
            printf("%s ", buff);
    }
    printf("\n");
}

void stampaFF(FILE *f1, FILE *f2)
{
    char buff[MAXL];

    while (!feof(f1))
    {
        fscanf(f1, "%s", buff);
        if (!tavolaEscapeFile(buff, f2))
            fprintf(f2, "%s ", buff);
    }
    printf("\n");
}
int pradq(int n)
{

    int i;

    if (n == 1)
        return 1;
    else if (!n)
        return 0;
    for (i = 1; i <= (n >> 1); i++)
        if (i * i == n)
            return i;
    if (i == n + 1)
    {
        printf("Non è un quadrato!\n");
        return 0;
    }
    return 0;
}

void cambiaSegno(int *segno)
{
    if (*segno == -1)
        *segno += 2;
    else
        *segno -= 2;
}

int intMax(int a, int b)
{
    if (a >= b)
        return a;
    else
        return b;
}

int intMin(int a, int b)
{
    if (a <= b)
        return a;
    else
        return b;
}

void initArray(int *v, int dim, int valore)
{
    int i;

    for (i = 0; i < dim; i++)
        v[i] = valore;
}

void fmMalloc(double ***f, unsigned int righe, unsigned int colonne)
{
    int i;

    *f = (double **)malloc(righe * sizeof(double *));
    for (i = 0; i < righe; i++)
        (*f)[i] = (double *)malloc(colonne * sizeof(double));
}

void intmMalloc(int ***n, unsigned int righe, unsigned int colonne)
{
    int i;

    *n = (int **)malloc(righe * sizeof(int *));
    for (i = 0; i < righe; i++)
        (*n)[i] = (int *)malloc(colonne * sizeof(int));
}

void uIntmMalloc(unsigned int ***n, unsigned int righe, unsigned int colonne)
{
    int i;

    *n = (unsigned int **)malloc(righe * sizeof(unsigned int *));
    for (i = 0; i < righe; i++)
        (*n)[i] = (unsigned int *)malloc(colonne * sizeof(unsigned int));
}

void mStrMalloc(char ***s, char *in, int size)
{
    int i, j;

    *s = (char **)malloc(size * sizeof(char *));
    for (i = 0; i < size; i++)
    {
        (*s)[i] = (char *)malloc(strlen(in) * sizeof(char));
        for (j = 0; j < strlen(in); j++)
        {
            (*s)[i][j] = in[j];
        }
    }
}

void mFree(void **ptr, int righe)
{
    int i;

    for (i = 0; i < righe; i++)
        free(ptr[i]);
    free(ptr);
}

int cercaIntero(int n, int *v, int dim)
{
    int i;

    for (i = 0; i < dim; i++)
        if (v[i] == n)
            return 1;
    return 0;
}

int cercaChar(char *s, char c)
{
    int i;

    for (i = 0; s[i] != 0; i++)
        if (s[i] == c)
            return 1;
    return 0;
}

void stampaSpeciale(char *in)
{
    if (!tavolaEscape(in))
        printf("%s", in);
}

void stampaCifreSp(char **s, int c, int nCifre)
{
    int i = nCifre - 1;

    while (s[i][c] == '0' && i)
        i--;
    while (i + 1)
    {
        stampaSpeciale(s[i]);
        i--;
    }
}

int aggiornaMStr(char **in, int c, int dim, char limit)
{
    int i, j;
    char add = 1;

    for (i = 0; i < dim; i++)
        if (in[i][c] < limit)
        {
            in[i][c] = in[i][c] + add;
            return 1;
        }
        else
        {
            in[i + 1][c]++;
            for (j = i; j >= 0; j--)
                in[j][c] = '0';
            return 1;
        }
    return 0;
}

void flusher(void)
{
    int c;

    do
    {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

void quit(void)
{
    char c;

    printf("Premere un tasto per uscire:\n");
    scanf("%c", &c);
}

clock_t active_sleep(int ms, int print)
{
    clock_t start = clock(), waited;

    if (print)
    {
        int flag = 1;
        clock_t print_waited;

        do
        {
            waited = clock() - start;
            if (flag && !(waited % 250))
            {
                print_waited = waited;
                flag = 0;
                putchar('.');
            }
            else if (print_waited - waited)
                flag = 1;
        } while (waited < ms);
        putchar('\n');
    }
    else
        do
            waited = clock() - start;
        while (waited < ms);

    return waited;
}

void help(void)
{
    FILE *fT;
    char c;

    fT = openFile("help.txt", "rt");
    stampaFC(fT);
    printf("Premere un tasto per continuare...\n");
    scanf("%c", &c);
}

#ifdef _WIN32
__int64 WinCrono(void)
{
    FILETIME ft;

    GetSystemTimeAsFileTime(&ft);

    return ((__int64)ft.dwHighDateTime << 32) + ft.dwLowDateTime;
}
#endif