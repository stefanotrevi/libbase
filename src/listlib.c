#include "baselib.h"
#include "listlib.h"

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdlib.h>
#include <stdio.h>

void listInit(list_t *l)
{
    *l = (list_t){0};
}

void listDelete(list_t *l)
{
    while (l->head)
    {
        list_node_t *temp = l->head->next;
        free(l->head);
        l->head = temp;
    }
    l->numel = 0;
    l->head = NULL;
}

list_t *listPut(list_t *l, int dato)
{
    //Per allocare il nodo nell'heap e non nello stack
    list_node_t *temp = malloc(sizeof(*temp));

    temp->data = dato;
    temp->next = l->head;
    l->head = temp;
    ++l->numel;

    return l;
}

void listPrint(list_t l)
{
    if (!l.head)
        puts("[]");
    else
    {
        list_node_t *next;

        putchar('[');
        while ((next = l.head->next))
        {
            printf("%d, ", l.head->data);
            l.head = next;
        }
        printf("%d", l.head->data);
        puts("]");
    }
}

list_node_t *listFind(list_t l, int n)
{
    while (l.head && l.head->data != n)
        l.head = l.head->next;

    return l.head;
}
