#include "rbtlib.h"

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdlib.h>
#include <stdio.h>

//I colori dei nodi
#define RB_BLACK 0
#define RB_RED 1

static rbt_node_t null_node_ctr = {0};
static rbt_node_t *const NULLNODE = &null_node_ctr;

void rbtInit(rbt_t *t)
{
    rbt_node_t *n = malloc(sizeof(*n));

    t->numel = 0;
    t->root = n;
    n->left = n->right = n->father = NULLNODE;
    n->color = RB_BLACK;
}

void rbtDelete(rbt_t *T)
{
    rbtDeleteNodes(T, T->root);
}

void rbtDeleteNodes(rbt_t *T, rbt_node_t *n)
{
    if (n->left != NULLNODE)
        rbtDeleteNodes(T, n->left);
    if (n->right != NULLNODE)
        rbtDeleteNodes(T, n->right);
    if (T->root == n)
        T->root = NULL;

    --T->numel;
    free(n);
}

void rbtRotL(rbt_t *T, rbt_node_t *n)
{
    rbt_node_t *temp = n->right;

    if (temp != NULLNODE)
    {
        //Il padre di n diventa padre di temp, che diventa padre di n
        rbt_node_t *fat = temp->father = n->father;
        n->father = temp;
        //Se era la radice, aggiorno
        if (fat == NULLNODE)
            T->root = temp;
        //altrimenti temp diventa figlio destro/sinistro
        else if (fat->left == n)
            fat->left = temp;
        else
            fat->right = temp;
        //Il figlio sinistro di z diventa figlio destro di x, che diventa figlio sinistro di z
        n->right = temp->left;
        if (temp->left != NULLNODE)
            temp->left->father = n;
        temp->left = n;
    }
}

void rbtRotR(rbt_t *T, rbt_node_t *n)
{
    rbt_node_t *temp = n->left;

    //L'algoritmo è del tutto simmetrico alla leftRotateRBT
    if (temp != NULLNODE)
    {
        rbt_node_t *fat = temp->father = n->father;

        n->father = temp;
        if (fat == NULLNODE)
            T->root = temp;
        else if (fat->left == n)
            fat->left = temp;
        else
            fat->right = temp;

        n->left = temp->right;
        if (temp->right != NULLNODE)
            temp->right->father = n;

        temp->right = n;
    }
}

rbt_t *rbtPut(rbt_t *T, int n)
{
    //Se l'albero è vuoto, inserisco nella radice
    if (!T->numel)
        T->root->key = n;
    else
    {
        rbt_node_t *temp = malloc(sizeof(*temp)), *fat;

        //Il nodo sarà certamente una foglia
        temp->key = n;
        fat = temp->father = T->root;
        temp->left = NULLNODE;
        temp->right = NULLNODE;
        temp->color = RB_RED;
        //Ciclo finché non ho posizionato il nodo
        while (fat->left != temp && fat->right != temp)
            //Se il padre è maggiore
            if (fat->key >= temp->key)
                //Se esiste la sinistra, confronto con la sinistra
                if (fat->left != NULLNODE)
                    fat = fat->left;
                //Altrimenti il nodo diventa la sinistra
                else
                    fat->left = temp;
            //Se il padre è minore, ed esiste la destra, confronto con la destra
            else if (fat->right != NULLNODE)
                fat = fat->right;
            //Altrimenti il nodo diventa la destra
            else
                fat->right = temp;
        rbtFix(T, temp);
    }
    T->numel++;

    return T;
}

void rbtFix(rbt_t *T, rbt_node_t *n)
{
    //Il padre di un nodo rosso deve essere nero
    while (n->father->color == RB_RED)
    {
        //Ribilanciamo l'albero
        rbtBalance(T, n);
        //La radice è sempre nera
        T->root->color = RB_BLACK;
    }
}

void rbtBalance(rbt_t *T, rbt_node_t *n)
{
    // switch destra/sinistra
    int lr;
    // padre, zio e nonno del nodo
    rbt_node_t *fat = n->father, *ben, *granpa = fat->father;

    // trovo lo zio del nodo (da grandi poteri derivano grandi responsabilità...)
    if (granpa->left == fat)
    {
        lr = 0;
        ben = granpa->right;
    }
    else
    {
        lr = 1;
        ben = granpa->left;
    }
    //Se lo zio è rosso, ho sbilanciato troppo l'albero
    if (ben->color == RB_RED)
    {
        //Il padre diventa nero
        fat->color = RB_BLACK;
        //Lo zio diventa nero
        ben->color = RB_BLACK;
        //Il nonno diventa rosso
        granpa->color = RB_RED;
        //Sposto il problema su di due livelli
        n = granpa;
    }
    //Zio sinistro
    else if (lr)
    {
        //Se il nodo è figlio destro, lo routo
        if (n == fat->right)
        {
            n = fat;
            rbtRotL(T, n);
        }
        fat->color = RB_BLACK;
        granpa->color = RB_RED;
        rbtRotR(T, granpa);
    }
    //Zio destro
    else
    {
        if (n == fat->left)
        {
            n = fat;
            rbtRotR(T, n);
        }
        fat->color = RB_BLACK;
        granpa->color = RB_RED;
        rbtRotL(T, granpa);
    }
}

rbt_t rbtPrintOrd(rbt_t T)
{
    if (T.numel)
    {
        putchar('[');
        rbtPrintOrec(T);
        putchar(']');
    }
    else
        printf("[]");

    putchar('\n');

    return T;
}

void rbtPrintOrec(rbt_t T)
{
    rbt_node_t *root = T.root;

    if ((T.root = root->left))
    {
        rbtPrintOrec(T);
        printf(", ");
    }

    printf("%d", root->key);

    if ((T.root = root->right))
    {
        printf(", ");
        rbtPrintOrec(T);
    }
}

rbt_t rbtPrintPre(rbt_t T)
{
    if (T.numel)
    {
        putchar('[');
        rbtPrintPrec(T);
        putchar(']');
    }
    else
        printf("[]");
    putchar('\n');

    return T;
}

void rbtPrintPrec(rbt_t T)
{
    rbt_node_t *root = T.root;

    printf("%d", root->key);

    if ((T.root = root->left))
    {
        printf(", ");
        rbtPrintPrec(T);
    }
    if ((T.root = root->right))
    {
        printf(", ");
        rbtPrintPrec(T);
    }
}

rbt_node_t *rbtFind(rbt_t T, int n)
{
    rbt_node_t *root = T.root;

    //Se è vuoto o se sono arrivato in fondo
    if (!T.numel || root == NULLNODE)
        return NULL;
    //Se coincide, ritorno la radice
    if (root->key == n)
        return root;
    //Altrimenti ripeto, a destra o a sinistra
    root = root->key > n ? root->left : root->right;

    return rbtFind(T, n);
}